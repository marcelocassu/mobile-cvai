package br.com.konvita.konvita_clientes.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class dbController {

    private SQLiteDatabase db;
    private dbo banco;

    public dbController(Context context) {
        banco = new dbo(context);
    }

    public String insereDado(String evento, String nomeevento, String ingresso,
                             String nomeIngresso, String tipoIngresso, String numlote,
                             String qtd, String preco) {

        ContentValues valores;
        long resultado;

        db = banco.getWritableDatabase();
        valores = new ContentValues();
        valores.put(dbo.EVENTO, evento);
        valores.put(dbo.NOMEEVENTO, nomeevento);
        valores.put(dbo.INGRESSO, ingresso);
        valores.put(dbo.NOMEINGRE, nomeIngresso);
        valores.put(dbo.TIPOINGRE, tipoIngresso);
        valores.put(dbo.NLOTE, numlote);
        valores.put(dbo.QTD, qtd);
        valores.put(dbo.PRECO, preco);


        resultado = db.insert(dbo.TABELA, null, valores);
        db.close();

        if (resultado == -1)
            return "Erro ao inserir registro";
        else
            return "Registro Inserido com sucesso";

    }

    public Cursor carregaDados(){
        Cursor cursor;
        String[] campos =  {"rowid _id", dbo.EVENTO,dbo.NOMEEVENTO};
        db = banco.getReadableDatabase();
        cursor = db.query(dbo.TABELA, campos, null, null, null, null, null, null);

        if(cursor!=null){
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public void deletatudo( ){
        //String where = dbo.TABELA + "=" + id;
        db = banco.getReadableDatabase();
        db.delete(dbo.TABELA,null,null);
        db.close();
    }

}






