package br.com.konvita.konvita_clientes.activity;

import android.graphics.Color;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.gregacucnik.EditTextView;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RedefinirSenha extends AppCompatActivity {

    SessionPrefs session;
    EditTextView etLP;
    Button btRecover;
    Response responses;
    String email, infojsonData, resultado;
    SweetAlertDialog pDialog, pDialogs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lostpassword);

        session = new SessionPrefs(this);

        if (android.os.Build.VERSION.SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        etLP = (EditTextView) findViewById(R.id.et_email);
        btRecover = (Button) findViewById(R.id.btRecover);

        btRecover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialogs = new SweetAlertDialog(RedefinirSenha.this, SweetAlertDialog.PROGRESS_TYPE);
                pDialogs.getProgressHelper().setBarColor(Color.parseColor("#303F9F"));
                pDialogs.setTitleText("Verificando ...");
                pDialogs.setCancelable(false);
                pDialogs.show();
                lp();
            }
        });
    }


public String lp() {

    try{
    //odraudeoiac@gmail_com
    email = etLP.getText().toLowerCase().replace("<<","");
    OkHttpClient client = new OkHttpClient();
    Request request = new Request.Builder().url(session.getGlobalURL()+"/api/esqueci-senha/" + email + "/").build(); // ENVIANDO DADOS PARA A URL ;
    Log.i("REX", "http://konvita.kplay.com.br/api/esqueci-senha/" + email + "/");
    responses = null;

    try {

        responses = client.newCall(request).execute();
        infojsonData = responses.body().string();

    } catch (IOException e) {
        e.printStackTrace();
    }

    try {

        JSONObject responseFull = new JSONObject(infojsonData);
        resultado = responseFull.getString("retorno");


        if (resultado.equals("true")) {
            pDialogs.dismiss();
            pDialog = new SweetAlertDialog(RedefinirSenha.this, SweetAlertDialog.SUCCESS_TYPE);
            pDialog.setTitleText("Sucesso");
            pDialog.setContentText("Verifique seu e-mail \n para visualizar a sua nova senha");
            pDialog.setConfirmText("Ok");
            pDialog.setCancelable(false);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismiss();
                    onBackPressed();
                    //startActivity(new Intent(RedefinirSenha.this, Login.class));
                    finish();
                }
            })
            .show();

        }else

            if (resultado.equals("false") || resultado.isEmpty()){
            pDialogs.dismiss();
            pDialog = new SweetAlertDialog(RedefinirSenha.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
            pDialog.setCustomImage(R.drawable.exclamation_icon);
            pDialog.setTitleText("Atenção");
            pDialog.setContentText("E-mail não cadastrado");
            pDialog.setCancelable(true);
            pDialog.show();
            Button viewGroup = (Button) pDialog.findViewById(cn.pedant.SweetAlert.R.id.confirm_button);
            viewGroup.setVisibility(View.GONE);
        }

    } catch (JSONException e) {

        Log.e("ERRO", "Não Encontrou Nenhum JSON", e);
    }

    } catch (Exception e) {
        e.printStackTrace();
    }
    return resultado;

    }

/*    private class LostPassword extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            pDialog = new SweetAlertDialog(RedefinirSenha.this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#303F9F"));
            pDialog.setTitleText("Autenticando ...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... result) {


            lp();

        return resultado;
        }


        @Override
        protected void onPostExecute(String resultado) {
            pDialog.dismiss();

            }


        }*/
    }





