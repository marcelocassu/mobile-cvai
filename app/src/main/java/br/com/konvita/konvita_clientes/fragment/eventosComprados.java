package br.com.konvita.konvita_clientes.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.adapter.eventosAdapterComprados;
import br.com.konvita.konvita_clientes.model.EventoC;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class eventosComprados extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

        SearchView sv;
        ListView Eventos;
        SweetAlertDialog pDialog;
        List<EventoC> eventosList = new ArrayList<EventoC>();
        eventosAdapterComprados adapter;

        String visi, user;
        TextView container;
        SwipeRefreshLayout swipeLayout;
        SessionPrefs sessionPrefs;

     public eventosComprados() { }

    @Override
    public void onStart() {
        super.onStart();

        //Receber o Json

    }

    @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);


        new readlocal().execute();

          // container = (TextView) getActivity().findViewById(R.id.vv);
          //  container.setVisibility(View.VISIBLE);

            sessionPrefs = new SessionPrefs(getActivity());

            //ListView
            Eventos = (ListView) getActivity().findViewById(R.id.listView02);

            //Adapter custom
            adapter = new eventosAdapterComprados(getActivity(), eventosList);
            swipeLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.Swipecontainer2);
            swipeLayout.setOnRefreshListener(this);
            swipeLayout.setRefreshing(true);


        sv = (SearchView)getActivity().findViewById(R.id.seacher2);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String arg0) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // TODO Auto-generated method stub

                adapter.getFilter().filter(query);

                return false;
            }
        });

        }



        private class readlocal extends AsyncTask<String, String, String> {


            @Override
            protected String doInBackground(String... strings) {
                String jsonData = null;
                try {

                    user = sessionPrefs.getId();


                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder().url(sessionPrefs.getGlobalURL()+"/api/meus-eventos/" + user + "/").build();
                    Response responses = null;

                    try {

                        responses = client.newCall(request).execute();
                        jsonData = responses.body().string();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    JSONArray obj_array = null;

                    try {


                        JSONObject Jasonobject = new JSONObject(jsonData);
                        obj_array = Jasonobject.getJSONArray("eventos");

                        eventosList.clear();
                        for (int i = 0; i < obj_array.length(); i++) {
                            JSONObject obj = obj_array.getJSONObject(i);
                            EventoC evento = new EventoC();
                            evento.setEvento(obj.optString("evento"));
                            evento.setSessao(obj.optString("sessao"));
                            evento.setNome(obj.optString("nome"));
                            evento.setCategoria(obj.optString("categoria"));
                            evento.setImage(obj.optString("foto"));
                            evento.setLogradouro(obj.optString("logradouro"));
                            evento.setEndereco(obj.optString("endereco"));
                            evento.setNumero(obj.optString("numero"));
                            evento.setBairro(obj.optString("bairro"));
                            evento.setCidade(obj.optString("cidade"));
                            evento.setUf(obj.optString("uf"));
                            eventosList.add(evento);
                            //Se tivesse uma variavel int
                            //eventos.setRating((Number) obj.get("rating")).doubleValue());
                        }

                    } catch (JSONException e) {



                        e.printStackTrace();
                    }

                   // adapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }


            @Override
            protected void onPreExecute() {   }


            @Override
            protected void onPostExecute(final String s) {
                super.onPostExecute(s);
                Eventos.setAdapter(adapter);
                if(swipeLayout.isRefreshing()) {
                    swipeLayout.setRefreshing(false);
                }
              //  if(visi.equals("0")) {
//                    container.setVisibility(View.INVISIBLE);
              //  }
               /* Eventos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Intent intent = new Intent(getActivity(), Ingressos.class);
                        //Envia as infomações via intent
                        intent.putExtra("nomeevento",  eventosList.get(position).getNome());
                        intent.putExtra("image", eventosList.get(position).getImage());
                        intent.putExtra("logradouro", eventosList.get(position).getLogradouro());
                        intent.putExtra("endereco", eventosList.get(position).getEndereco());
                        intent.putExtra("numero", eventosList.get(position).getNumero());
                        intent.putExtra("bairro", eventosList.get(position).getBairro());
                        intent.putExtra("cidade", eventosList.get(position).getCidade());
                        intent.putExtra("uf", eventosList.get(position).getUf());
                        startActivity(intent);

                    }
                });*/

            }


        }

   // void onBackStackChanged(){

    //}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.eventos_comprados, container, false);
    }


    @Override
    public void onRefresh() {
        new readlocal().execute();

    }



}


