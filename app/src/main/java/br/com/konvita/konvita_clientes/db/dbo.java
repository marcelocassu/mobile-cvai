package br.com.konvita.konvita_clientes.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;


public class dbo extends SQLiteOpenHelper implements BaseColumns {


    static final String NOME_BANCO = "IngressoDB.db";
     static final String TABELA = "Ingresso";
     public static final String EVENTO = "Evento";
     public static final String NOMEEVENTO = "nomeEvento";
     static final String INGRESSO = "ingresso";
     static final String NOMEINGRE = "nomeIngresso";
     static final String TIPOINGRE = "tipoIngresso";
     static final String NLOTE = "numlote";
     static final String QTD = "quantidade";
     static final String PRECO = "preco";
    private static final int VERSAO = 1;


    public dbo(Context context) {
        super(context, NOME_BANCO, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
         String sql = "CREATE TABLE IF NOT EXISTS "+TABELA+" ("+EVENTO+" VARCHAR(50), "+NOMEEVENTO+" VARCHAR(50)," +
                 " "+INGRESSO+" VARCHAR(50), "+NOMEINGRE+" VARCHAR(50),"+TIPOINGRE+" VARCHAR(50), "+NLOTE+" VARCHAR(50)," +
                 " "+QTD+" VARCHAR(50), "+PRECO+" VARCHAR(50))";
       // Log.i("APX", sql);
        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABELA);
        onCreate(db);
        //db.close();
        }
}


