package br.com.konvita.konvita_clientes.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gregacucnik.EditTextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Login extends AppCompatActivity {

    SessionPrefs session;
    TextView btlp, btcadastro;
    EditTextView etUsername;
    EditTextView etPassword;
    String user, pass, infojsonData, resultado,
    tipousu, atv;
    Integer auth;


    @InjectView(R.id.bt_go)
    Button btGo;
    @InjectView(R.id.cv)
    CardView cv;
  //  @InjectView(R.id.fab)
  //  FloatingActionButton fab;

    Response responses;
    SweetAlertDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);
        session = new SessionPrefs(this);


        etUsername = (EditTextView) findViewById(R.id.et_username);
        etPassword = (EditTextView) findViewById(R.id.et_password);
        btlp = (TextView) findViewById(R.id.btlp);
        btcadastro = (TextView) findViewById(R.id.btcadastro);




        btcadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(Login.this, "Inplementando ainda o botão", Toast.LENGTH_LONG).show();
               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(session.getGlobalURL()+"/usuario/tipo"));
                startActivity(browserIntent);
            }
        });

        btlp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(Login.this, "Inplementando ainda o botão", Toast.LENGTH_LONG).show();
                //odraudeoiac@gmail_com
                startActivity(new Intent(Login.this, RedefinirSenha.class));

            }
        });


        if (session.loggedin()) {
            startActivity(new Intent(Login.this, Home.class));
            finish();
        }

    }

    //Verifica se tem internet disponivel
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();

        /* if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }*/
    }

    //Ação do botão entrar e do flutuante
    @OnClick({R.id.bt_go, /*R.id.fab*/})
    public void onClick(View view) {
        switch (view.getId()) {

            // ESSA PARTE NÃO É NECESSÁRIA NA VERSÃO CHECK-IN ;

         /*   case R.id.fab:
                //getWindow().setExitTransition(null);
                //getWindow().setEnterTransition(null);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, fab, fab.getTransitionName());
                    startActivity(new Intent(this, Credits.class), options.toBundle());
                } else {
                    startActivity(new Intent(this, Credits.class));
                }
                break;*/

            // FIM DA PARTE QUE NÃO É NECESSÁRIA NA VERSÃO CHECK-IN ;

            // btn de Login ;
            case R.id.bt_go:
                if (isOnline()) {
                    //user = etUsername.getText();
                    //pass = etPassword.getText();
                    //user = "odraudeoiac@hotmail.com";
                    //pass = "huehuehue";
                    //user = "marcos@cvai.com";
                    //pass = "marcoscvai";
                    //user = "teste@konvita.com";
                    //pass = "konvita2017";
                    user = "bmatheusc@gmail.com";
                    pass = "123456789";
                    if(user.equals("") && pass.equals("") ) {

                        pDialog = new SweetAlertDialog(Login.this, SweetAlertDialog.WARNING_TYPE);
                        pDialog.setTitleText("Atenção");
                        pDialog.setContentText("Preencha os campos para continuar");
                        pDialog.setConfirmText("Entendi");
                        pDialog.setCancelable(false);
                        pDialog.show();

                    }else{
                        LogIn AsyncTask = new LogIn();
                        AsyncTask.execute();
                    }

                } else {
                    pDialog = new SweetAlertDialog(Login.this, SweetAlertDialog.WARNING_TYPE);
                    pDialog.setTitleText("Atenção");
                    pDialog.setContentText("Conecte se a internet para realizar seu Login");
                    pDialog.setConfirmText("Entendi");
                    pDialog.setCancelable(false);
                    pDialog.show();
                }


                break;
        }
    }


    // Tarefa Dessincronizada Para Verificar Login e Dados no SQL Server {} ;
    private class LogIn extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

            pDialog = new SweetAlertDialog(Login.this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#303F9F"));
            pDialog.setTitleText("Autenticando ...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... v) {


            Login1();

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            pDialog.dismiss();



            switch (atv) {
                case "1":
                    if (tipousu.equals("Física")) {
                        session.setLoggedin(true);
                        getnotification();
                        Open();
                    }
                    if (tipousu.equals("Jurídica")) {
                        Toast.makeText(Login.this, "Você deve utilizar o aplicativo #CVai Check-In", Toast.LENGTH_LONG).show();
                    }

                    break;
                case "0":
                    if (tipousu.equals("Física")) {
                        Toast.makeText(Login.this, "Entre no seu e-mail e ative sua conta", Toast.LENGTH_LONG).show();
                    }


                    if (tipousu.equals("Jurídica")) {
                        Toast.makeText(Login.this, "Você deve Utilizar o aplicativo de gereciamento e ativar sua conta", Toast.LENGTH_LONG).show();
                    }
                    break;
                case "2":
                    Close();
                    break;
            }

        }
    }


    //Recupera as informações pessoais do cabra
    public String InfosUsu() {

        try {

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(session.getGlobalURL()+"/api/info-usuario/" + session.getId() + "/").build(); // ENVIANDO DADOS PARA A URL ;
            responses = null;

            try {

                responses = client.newCall(request).execute();
                infojsonData = responses.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }


            try {


                JSONObject responseFull = new JSONObject(infojsonData);
                JSONArray obj_array;
                resultado = responseFull.getString("retorno");

                if (resultado.equals("true")) {
                    obj_array = responseFull.getJSONArray("usuario");

                    for (int i = 0; i < obj_array.length(); i++) {
                        JSONObject obj = obj_array.getJSONObject(i);
                        session.setNome(obj.optString("nome"));
                        session.setSobrenome(obj.optString("sobrenome"));
                        session.setCpf(obj.getString("cpf"));
                        session.setRg(obj.optString("rg"));
                        session.setEmail(obj.optString("email"));
                        session.setDdd(obj.optString("dddCelular"));
                        session.setCelular(obj.optString("celular"));
                        session.setFotoperfil(obj.optString("foto"));
                        session.setGenero(obj.optString("sexo"));
                        session.setDt(obj.optString("dataNascimento"));

                    }

                } else {
                    Log.i("REX", "ERRO");
                }

            } catch (JSONException e) {
                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //Verifica se o login existe

    private String Login1() {


        String resultado = null;

        try {
            RequestBody formBody = new FormBody.Builder()
                    .add("email", user)
                    .add("senha", pass)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(session.getGlobalURL()+"/api/login/")
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            String json = response.body().string();
            Log.i("TESTELOGIN", json);

            try {

                // Aqui pega o retorno ({"retorno":true,"msg":[{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}]});

                JSONObject responseFull = new JSONObject(json);
                JSONArray obj_array;

                // Aqui separo a mensagem de validação ("retorno":true);
                resultado = responseFull.getString("retorno");

               if (resultado.equals("true")) {


                   obj_array = responseFull.getJSONArray("usuario");

                   for (int i = 0; i < obj_array.length(); i++) {
                       JSONObject obj = obj_array.getJSONObject(i);
                       session.setId(obj.optString("usuario"));
                       tipousu = (obj.getString("tipo"));
                       atv = (obj.optString("ativocad"));
                       InfosUsu();
                   }


               }else {

                    atv = "2";

                }


            } catch (JSONException e) {

                atv = "2";

                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);

            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    //Gera a notificação
    public void getnotification() {

        NotificationManager notificationmgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);


        Intent intent = new Intent(this, Home.class);

        //intent.putExtra("id", sessao.getId());

        PendingIntent pintent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);



        Notification notif = new Notification.Builder(this)
                .setSmallIcon(R.drawable.cvai_textlogo)
                .setContentTitle("Olá, "+session.getNome() + " " + session.getSobrenome()+" #CVai?")
                .setContentText("Muitos eventos estão a sua espera.")
                .setAutoCancel(true)
                .setContentIntent(pintent)
                .build();


        notificationmgr.notify(0, notif);

    }

    public void Open() {

        Intent intent = new Intent(getApplicationContext(), Home.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        finish();


           /* Explode explode = new Explode();
            explode.setDuration(500);

            getWindow().setExitTransition(explode);
            getWindow().setEnterTransition(explode);
            ActivityOptionsCompat oc2 = ActivityOptionsCompat.makeSceneTransitionAnimation(this);


            user = etUsername.getText();
            pass = etPassword.getText();

            Intent i2 = new Intent(this,Home.class);
            startActivity(i2, oc2.toBundle());*/

    }

    public void Close() {
        SweetAlertDialog sweetAlertDialogView;
        sweetAlertDialogView = new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        sweetAlertDialogView.setCustomImage(R.drawable.exclamation_icon); // PASSAR PARA O RENAN OU JÉSSICA CRIAR UMA IMAGEM PARA O ALERTA;
        sweetAlertDialogView.setTitleText("Alerta")
                .setContentText("Usuário e/ou senha incorretos");
        sweetAlertDialogView.show();
        Button viewGroup = (Button) sweetAlertDialogView.findViewById(cn.pedant.SweetAlert.R.id.confirm_button);
        //if (viewGroup != null) {
        //Log.e(TAG, "showErrorMsg: Button view Found yep");
        //viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.colorAlert));
        viewGroup.setVisibility(View.GONE);
        pDialog.dismiss();

    }
}
