package br.com.konvita.konvita_clientes.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.SearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.adapter.fileiraCinemaAdapter;
import br.com.konvita.konvita_clientes.model.PoltronasLivres;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PoltronasSelect extends AppCompatActivity {

    ArrayList<PoltronasLivres> poltronasLivresList = new ArrayList<PoltronasLivres>();
    fileiraCinemaAdapter adapter;
    SessionPrefs session;
    Response response;
    GridView gridView;
    SearchView sv;
    SweetAlertDialog SAD;
    Button Finalbuy, busyPol, vmp;
    String json, sessao, auth, erro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poltrona_select);

        Finalbuy = (Button) findViewById(R.id.finalbuy);
        busyPol = (Button) findViewById(R.id.busy);
        vmp = (Button) findViewById(R.id.vmp);
        gridView = (GridView) findViewById(R.id.lkl);
        session = new SessionPrefs(this);
        session.setFlag("1");
        adapter = new fileiraCinemaAdapter(this, poltronasLivresList);
        Bundle bundle = getIntent().getExtras();
        sessao = bundle.getString("sessao");
        session.setC(sessao);

        new CarregaGrid().execute();


        Finalbuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new addcc().execute();

            }
        });

        busyPol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(PoltronasSelect.this, br.com.konvita.konvita_clientes.activity.PoltronasOcupadas.class);
                intent.putExtra("sessao", sessao);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });

        vmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), CineMp.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        sv = (SearchView) findViewById(R.id.seacherPol);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String arg0) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // TODO Auto-generated method stub

                adapter.getFilter().filter(query);

                return false;
            }
        });
    }


    //Carrega o GridView
    /**
     * Subtituir a API para assentos livres
     */

    private class CarregaGrid extends AsyncTask<Void, Void, Void> {
    SpotsDialog dialog = new SpotsDialog(PoltronasSelect.this, R.style.Custom);
        @Override
        protected void onPreExecute() {
            dialog.setCancelable(false);
            dialog.show();
        }


        @Override
        protected Void doInBackground(Void... v) {

            addGrid();
            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            if(dialog.isShowing()){
                gridView.setAdapter(adapter);
                dialog.dismiss();
            }


        }
    }


    public String addGrid() {

        String resultado ;

        try {
            RequestBody formBody = new FormBody.Builder()
                    .add("sessao", sessao)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(session.getGlobalURL() + "/api-cinema/assentos-ocupados")
                    .post(formBody)
                    .build();

            response = client.newCall(request).execute();
            json = response.body().string();

            try {

                JSONObject responseFull = new JSONObject(json);
                JSONArray obj_array;

                resultado = responseFull.getString("retorno");

                if (resultado.equals("true")) {


                    String resultado2 = responseFull.getString("ocupadas");
                    obj_array = new JSONArray(resultado2);
                    poltronasLivresList.clear();
                     for (int i = 0; i < obj_array.length(); i++) {

                        //Forma principal
                         PoltronasLivres fi = new PoltronasLivres();
                         fi.setF(obj_array.optString(i));
                         fi.setF("A14");
                         poltronasLivresList.add(fi);

                         //Outra Forma
                         //poltronasLivresList.add(new PoltronasLivres(obj_array.optString(i)));
                    }
                    auth = "1";
                }

            } catch (JSONException e) {
                erro = json;
                auth = "0";
                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    //Adiciona no carrinho

    private class addcc extends AsyncTask<Void, Void, Void> {
    SpotsDialog dialog = new SpotsDialog(PoltronasSelect.this, R.style.Custom);
        @Override
        protected void onPreExecute() {
            dialog.setCancelable(false);
            dialog.show();
        }


        @Override
        protected Void doInBackground(Void... v) {

            addcadeiracarrinho();


            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            if(dialog.isShowing()){
                dialog.dismiss();
            }
            if (auth.equals("1")) {
                Intent intent = new Intent(getApplicationContext(), Carrinho.class);
                startActivity(intent);
            } else if (auth.equals("0")) {
                SAD = new SweetAlertDialog(PoltronasSelect.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                SAD
                        .setCustomImage(R.drawable.exclamation_icon)
                        .setTitleText("Atenção")
                        .setContentText("Deseja apagar seu carrinho, para adicionar novos itens?")
                        .setConfirmText("Sim")
                        .setCancelText("Não")

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                new removeitem().execute();
                                sweetAlertDialog.dismiss();

                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
                SAD.setCancelable(false);
            }else if(auth.equals("2")){
                SAD = new SweetAlertDialog(PoltronasSelect.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                SAD
                        .setCustomImage(R.drawable.exclamation_icon)
                        .setTitleText("Atenção")
                        .setContentText("Selecione ao menos uma poltrona para continuar")
                        .setConfirmText("Entendi")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
                SAD.setCancelable(false);
            }
        }
    }

    public String addcadeiracarrinho() {


        String resultado;

        try {
            RequestBody formBody = new FormBody.Builder()
                    .add("sessao", sessao)
                    .add("usuario", session.getId())
                    .build();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(session.getGlobalURL() + "/api-cinema/add-carrinho")
                    .post(formBody)
                    .build();

            response = client.newCall(request).execute();
            json = response.body().string();
            Log.i("Cadeiras1", json);

            try {

               JSONObject responseFull = new JSONObject(json);
                //JSONArray obj_array;

                resultado = responseFull.getString("retorno");

                if (resultado.equals("true")) {
                    session.setFlag("1");
                    auth = "1";

                } else if (resultado.equals("false")) {

                    auth = "0";
                }

            } catch (JSONException e) {
                auth = "2";
                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);

            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



    //REMOVE DO CARRINHO
    private class removeitem extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
        }


        @Override
        protected Void doInBackground(Void... v) {

            Removeitem();


            return null;

        }

        @Override
        protected void onPostExecute(Void result) {

            new SweetAlertDialog(PoltronasSelect.this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Carrinho Apagado")
                    .setConfirmText("Ok")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            new addcc().execute();
                            sweetAlertDialog.dismiss();

                        }
                    }).show();

        }
    }

    public void Removeitem() {


        try {

            OkHttpClient client = new OkHttpClient();
             Request request = new Request.Builder().url(session.getGlobalURL() + "/api/limpar-carrinho/" + session.getId() + "").build();
            Response responses = null;
            String jsonData = null;

            try {

                responses = client.newCall(request).execute();
                jsonData = responses.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }


            try {


               JSONObject responseFull = new JSONObject(jsonData);
                //JSONArray obj_array ;

               String resultado = responseFull.getString("retorno");

               if (resultado.equals("true")) {
                    session.setEvetemp("");
                    session.setFotoevetemp("");
                    session.setVlttemp("");

                }

            } catch (JSONException e) {
                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
