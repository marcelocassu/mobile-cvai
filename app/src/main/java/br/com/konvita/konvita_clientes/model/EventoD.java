package br.com.konvita.konvita_clientes.model;


import android.widget.ListView;

import java.util.List;

public class EventoD {


    private String nome;
    private String descricao, dhini, dhfim, dhinicheck, dhfimcheck;
    private String image, logradouro, uf, local,
            endereco, numero, bairro, cidade, idevento, categoria;
    private String nomeEvento, nIngresso, ctgIngresso,
            tipoIngresso, numLote, qtd, preco;

    public List<Ingresso> ingressos;

    public EventoD(){

    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getIdevento() {
        return idevento;
    }

    public void setIdevento(String idevento) {
        this.idevento = idevento;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDhini() {
        return dhini;
    }

    public void setDhini(String dhini) {
        this.dhini = dhini;
    }

    public String getDhfim() {
        return dhfim;
    }

    public void setDhfim(String dhfim) {
        this.dhfim = dhfim;
    }

    public String getDhinicheck() {
        return dhinicheck;
    }

    public void setDhinicheck(String dhinicheck) {
        this.dhinicheck = dhinicheck;
    }

    public String getDhfimcheck() {
        return dhfimcheck;
    }

    public void setDhfimcheck(String dhfimcheck) {
        this.dhfimcheck = dhfimcheck;
    }


/*
    public String getNomeEve() {
        return nomeEvento;
    }

    public void setNomeEve(String nomeEvento) {
        this.nomeEvento = nomeEvento;
    }

    public String getnIngresso() {
        return nIngresso;
    }

    public void setnIngresso(String nIngresso) {
        this.nIngresso = nIngresso;
    }

    public String getCtgIngresso() {
        return ctgIngresso;
    }

    public void setCtgIngresso(String ctgIngresso) {
        this.ctgIngresso = ctgIngresso;
    }

    public String getTipoIngresso() {
        return tipoIngresso;
    }

    public void setTipoIngresso(String tipoIngresso) {
        this.tipoIngresso = tipoIngresso;
    }

    public String getNumLote() {
        return numLote;
    }

    public void setNumLote(String numLote) {
        this.numLote = numLote;
    }

    public String getQtd() {
        return qtd;
    }

    public void setQtd(String qtd) {
        this.qtd = qtd;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }
*/



}
