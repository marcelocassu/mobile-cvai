package br.com.konvita.konvita_clientes.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.activity.PoltronasSelect;
import br.com.konvita.konvita_clientes.activity.CineMp;
import br.com.konvita.konvita_clientes.model.SessaoFilme;

import static br.com.konvita.konvita_clientes.R.layout.sessaofilme_row;


public class sessaoFilmeAdapter extends BaseAdapter {

    private List<SessaoFilme> sessaoFilmeList;
    private Context context;


    public sessaoFilmeAdapter(Context c, List<SessaoFilme> sessaoFilmeItem) {
        this.sessaoFilmeList = sessaoFilmeItem;
        this.context = c;

    }

    @Override
    public int getCount() {
        return sessaoFilmeList.size();
    }

    @Override
    public Object getItem(int position) {
        return sessaoFilmeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return sessaoFilmeList.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(sessaofilme_row, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final SessaoFilme currentItem = sessaoFilmeList.get(position);

        String date_before = currentItem.getDinicio();
        String date_after = formateDateFromstring("yyyy-MM-dd", "d MMMM yyyy", date_before);
        viewHolder.date.setText(date_after);
        viewHolder.itemName.setText(currentItem.getNomefantasia());
        String hora = currentItem.getHorario().substring(11,16)+"h";
        viewHolder.horario.setText(hora);
        viewHolder.sala.setText(currentItem.getSala());
        viewHolder.audio.setText(currentItem.getAudio());
        viewHolder.tela.setText(currentItem.getTela());

        viewHolder.comprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, PoltronasSelect.class);
                intent.putExtra("sessao", currentItem.getSessao());
                context.startActivity(intent);
            }
        });

        viewHolder.vc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, CineMp.class);

                context.startActivity(intent);


            }
        });

        return convertView;
    }

    private static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.i("REX", "ParseException - dateFormat");
        }

        return outputDate;

    }

    //ViewHolder inner class
    private class ViewHolder {
        TextView date;
        TextView itemName;
        TextView horario;
        TextView sala;
        TextView audio;
        TextView tela;
        Button  comprar;
        Button  vc;

        private ViewHolder(View view) {
            date = (TextView) view.findViewById(R.id.dataini);
            itemName = (TextView) view.findViewById(R.id.nomefantasia);
            horario = (TextView) view.findViewById(R.id.horaini);
            sala = (TextView) view.findViewById(R.id.sala);
            audio = (TextView) view.findViewById(R.id.audio);
            tela = (TextView) view.findViewById(R.id.telacine);
            comprar = (Button)view.findViewById(R.id.comprarsessao);
            vc = (Button)view.findViewById(R.id.vc);


        }
    }
}

