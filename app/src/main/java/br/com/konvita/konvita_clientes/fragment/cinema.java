package br.com.konvita.konvita_clientes.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.adapter.cinemaAdapter;
import br.com.konvita.konvita_clientes.adapter.eventosAdapterComprados;
import br.com.konvita.konvita_clientes.model.CinemaInfo;
import br.com.konvita.konvita_clientes.model.EventoC;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class cinema extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

        SearchView sv;
        GridView Filmes;
        SweetAlertDialog pDialog;
        List<CinemaInfo> cinemaList = new ArrayList<CinemaInfo>();
        cinemaAdapter adapter;
        int user;
        String jsonData = null;
        SwipeRefreshLayout swipeLayout;
        SessionPrefs sessionPrefs;

     public cinema() { }

    @Override
    public void onStart() {
        super.onStart();

        //Receber o Json
        readlocal readlocal = new readlocal();
        readlocal.execute();
    }

    @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);


            sessionPrefs = new SessionPrefs(getActivity());

            //ListView
            Filmes = (GridView) getActivity().findViewById(R.id.cinemaList);

            //Adapter custom
            adapter = new cinemaAdapter(getActivity(), cinemaList);
            swipeLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.Swipecontainer3);
            swipeLayout.setOnRefreshListener(this);
            swipeLayout.setRefreshing(true);



        sv = (SearchView)getActivity().findViewById(R.id.seacher3);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String arg0) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // TODO Auto-generated method stub

                adapter.getFilter().filter(query);


                return false;
            }
        });

        }



        private class readlocal extends AsyncTask<String, String, String> {


            @Override
            protected String doInBackground(String... strings) {

                try {



                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder().url(sessionPrefs.getGlobalURL()+"/api-cinema/filmes").build();
                    Response responses = null;

                    try {

                        responses = client.newCall(request).execute();
                        jsonData = responses.body().string();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    try {

                        JSONObject Jsonobject = new JSONObject(jsonData);
                        JSONArray obj_array;
                        String Resultado = Jsonobject.getString("retorno");

                        if(Resultado.equals("true")){

                        obj_array = Jsonobject.getJSONArray("filmes");
                        cinemaList.clear();
                        for (int i = 0; i < obj_array.length(); i++) {
                            JSONObject obj = obj_array.getJSONObject(i);

                            CinemaInfo cinemaInfo = new CinemaInfo();

                            cinemaInfo.setFilmeid(obj.optString("filme"));
                            cinemaInfo.setNome(obj.optString("nome"));
                            cinemaInfo.setSinopse(obj.optString("sinopse"));
                            cinemaInfo.setClassific(obj.optString("classificacao"));
                            cinemaInfo.setDiretor(obj.optString("diretor"));
                            cinemaInfo.setFoto(obj.optString("foto"));
                            cinemaInfo.setNomeoriginal(obj.optString("nomeoriginal"));
                            cinemaInfo.setTempo(obj.optString("tempo"));
                            cinemaInfo.setTrailer(obj.optString("trailer"));
                            cinemaList.add(cinemaInfo);

                        }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                   // adapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }


            @Override
            protected void onPreExecute() {   }


            @Override
            protected void onPostExecute(final String s) {
                super.onPostExecute(s);
                Filmes.setAdapter(adapter);
                swipeLayout.setRefreshing(false);

               /* Eventos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Intent intent = new Intent(getActivity(), Ingressos.class);
                        //Envia as infomações via intent
                        intent.putExtra("nomeevento",  eventosList.get(position).getNome());
                        intent.putExtra("image", eventosList.get(position).getImage());
                        intent.putExtra("logradouro", eventosList.get(position).getLogradouro());
                        intent.putExtra("endereco", eventosList.get(position).getEndereco());
                        intent.putExtra("numero", eventosList.get(position).getNumero());
                        intent.putExtra("bairro", eventosList.get(position).getBairro());
                        intent.putExtra("cidade", eventosList.get(position).getCidade());
                        intent.putExtra("uf", eventosList.get(position).getUf());
                        startActivity(intent);

                    }
                });*/

            }


        }

   // void onBackStackChanged(){

    //}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.cinema, container, false);
        rootView.setVerticalScrollBarEnabled(true);

        return rootView;
    }


    @Override
    public void onRefresh() {
        new readlocal().execute();

    }



}


