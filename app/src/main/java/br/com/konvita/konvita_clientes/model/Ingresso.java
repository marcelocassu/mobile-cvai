package br.com.konvita.konvita_clientes.model;



public class Ingresso {

    private static Ingresso instance;

    private String idevento, nomeEvento, nIngresso, ctgIngresso,
    tipoIngresso, numLote, qtd, preco;


    public Ingresso() {}


    public static Ingresso getInstance() {
        if (instance == null)
            instance = new Ingresso();
        return instance;
    }

    public String getIdevento() {
        return idevento;
    }

    public void setIdevento(String idevento) {
        this.idevento = idevento;
    }

    public String getNomeEve() {
        return nomeEvento;
    }

    public void setNomeEve(String nomeEvento) {
        this.nomeEvento = nomeEvento;
    }

    public String getnIngresso() {
        return nIngresso;
    }

    public void setnIngresso(String nIngresso) {
        this.nIngresso = nIngresso;
    }

    public String getCtgIngresso() {
        return ctgIngresso;
    }

    public void setCtgIngresso(String ctgIngresso) {
        this.ctgIngresso = ctgIngresso;
    }

    public String getTipoIngresso() {
        return tipoIngresso;
    }

    public void setTipoIngresso(String tipoIngresso) {
        this.tipoIngresso = tipoIngresso;
    }

    public String getNumLote() {
        return numLote;
    }

    public void setNumLote(String numLote) {
        this.numLote = numLote;
    }

    public String getQtd() {
        return qtd;
    }

    public void setQtd(String qtd) {
        this.qtd = qtd;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

}

