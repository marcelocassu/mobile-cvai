package br.com.konvita.konvita_clientes.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.activity.Carrinho;
import br.com.konvita.konvita_clientes.activity.Filme;
import br.com.konvita.konvita_clientes.model.CinemaInfo;
import br.com.konvita.konvita_clientes.model.EventoC;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class cinemaAdapter extends BaseAdapter implements Filterable {

    SessionPrefs session;
    private CustomFilter filter;
    private List<CinemaInfo> filterList;
    private List<CinemaInfo> filmes;
    private Context context;


    public cinemaAdapter(Context c, List<CinemaInfo> filmesitem) {
        this.filterList = filmesitem;
        this.filmes = filmesitem;
        this.context = c;
    }

    @Override
    public int getCount() {
        return filmes.size(); //returns total item in the list
    }

    @Override
    public Object getItem(int position) {
        return filmes.get(position); //returns the item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return filmes.indexOf(getItem(position));
    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.cinema_row, parent, false);
            viewHolder = new cinemaAdapter.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (cinemaAdapter.ViewHolder) convertView.getTag();
        }

         final CinemaInfo currentItem = (CinemaInfo) getItem(position);
        final SessionPrefs session = new SessionPrefs(context);

        //Ativa desincronizadamente a adição no carrinho
        class infofilme extends AsyncTask<Void, Void, Void> {
            @Override
            protected void onPreExecute() {
            }

            @Override
            protected Void doInBackground(Void... v) {

                try {
                    RequestBody formBody = new FormBody.Builder()
                            .add("filme", currentItem.getFilmeid())
                            .build();

                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(session.getGlobalURL()+"/api-cinema/filme")
                            .post(formBody)
                            .build();

                    Response response = client.newCall(request).execute();
                    String json = response.body().string();



                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;

            }

            @Override
            protected void onPostExecute(Void result) {

            }
        }

        Picasso.with(context).load(session.getGlobalURL()+"/imagens/bannerfilmes/" + currentItem.getFoto())
                .centerCrop()
                .resize(480, 720)
                .error(R.drawable.unk)
                .into(viewHolder.avatar);
        viewHolder.itemName.setText(currentItem.getNome());
        viewHolder.itemDescription.setText("CINEMAX");//nomde do cine
        viewHolder.itempreco.setText("Piraju - SP");// cidade


        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

               new infofilme().execute();
                // Send single item click data to SingleItemView Class
                Intent intent = new Intent(context, Filme.class);
                //Envia as infomações
                intent.putExtra("nomefilme", filmes.get(position).getNome());
                intent.putExtra("idfilm", filmes.get(position).getFilmeid());
                intent.putExtra("imagem", filmes.get(position).getFoto());
                intent.putExtra("classic", filmes.get(position).getClassific());
                intent.putExtra("diretor", filmes.get(position).getDiretor());
                intent.putExtra("nomeoriginal", filmes.get(position).getNomeoriginal());
                intent.putExtra("sinopse", filmes.get(position).getSinopse());
                intent.putExtra("tempo", filmes.get(position).getTempo());
                intent.putExtra("trailer", filmes.get(position).getTrailer());
                context.startActivity(intent);
            }
        });


        return convertView;
    }

        @Override
    public Filter getFilter() {

        if (filter == null) {
            filter = new cinemaAdapter.CustomFilter();
        }
        return filter;
    }

    //Custom Filter Inner Class
    private class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                //CONSTARINT TO UPPER
                constraint = constraint.toString().toUpperCase();
                final ArrayList<CinemaInfo> filters = new ArrayList<>();


                filters.clear();
                //get specific items
                for (int i = 0; i < filterList.size(); i++) {
                    if (filterList.get(i).getNome().toUpperCase().contains(constraint)) {
                        filters.add(filterList.get(i));
                    }
                }
                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = filterList.size();
                results.values = filterList;

            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filmes = (ArrayList<CinemaInfo>) results.values;

            notifyDataSetChanged();
        }
    }

    //ViewHolder inner class
    private class ViewHolder {
        TextView itemName;
        TextView itemDescription;
        TextView itempreco;
        ImageView avatar;

        private ViewHolder(View view) {
            itemName = (TextView) view.findViewById(R.id.nombre);
            itemDescription = (TextView) view.findViewById(R.id.descripcion);
            itempreco = (TextView) view.findViewById(R.id.precio);
            avatar = (ImageView) view.findViewById(R.id.imagen);
        }
    }
}


