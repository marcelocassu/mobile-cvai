package br.com.konvita.konvita_clientes.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.adapter.ingressosCAdapter;
import br.com.konvita.konvita_clientes.model.IngressoC;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class EventosCIngressos extends AppCompatActivity {

    String nome, image, idevento, cidade, estado;
    SweetAlertDialog pDialog;
    double lat, lng;
    TextView tvnome;
    TextView verMapa;
    TextView tv_dhini;
    TextView tv_local;
    TextView valor;
    ListView IngressoL;
    ingressosCAdapter adapter;
    List<IngressoC> ingressoCList = new ArrayList<IngressoC>();

    SessionPrefs session;

    @Override
    protected void onStart() {
        super.onStart();
        new readIngressos().execute();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos_ct);


        session = new SessionPrefs(this);
        adapter = new ingressosCAdapter(this, ingressoCList);


        tvnome = (TextView) findViewById(R.id.tvevento2);
        ImageView banner = (ImageView) findViewById(R.id.banner);
        verMapa = (Button) findViewById(R.id.vmp);
        tv_dhini = (TextView) findViewById(R.id.dhini);
        IngressoL = (ListView) findViewById(R.id.ingressosct);
        tv_local = (TextView) findViewById(R.id.local);
        valor = (TextView) findViewById(R.id.valor);


        //Recebe os dados do Fragment EventoD
        Bundle bundle = getIntent().getExtras();
        nome = bundle.getString("nomeevento");
        image = bundle.getString("image");
        idevento = bundle.getString("es");
        //Rua//endereco//numero
        String logradouro = bundle.getString("logradouro");
        String endereco = bundle.getString("endereco");
        String numero = bundle.getString("numero");
        String bairro = bundle.getString("bairro");
        cidade = bundle.getString("cidade");
        estado = bundle.getString("uf");
        //Data e hora
        //String dini1 = bundle.getString("datainicio");
        //String dfim1 = bundle.getString("datafim");
        //String hini = bundle.getString("horainicio");
        //String hfim = bundle.getString("horafim");


        //Seta as variaveis com seus devidos valores.
        if (session.getFlag().equals("0")) {
            Picasso.with(EventosCIngressos.this).load(image).resize(200, 320).error(R.drawable.error).into(banner);
        } else {
            Picasso.with(EventosCIngressos.this).load(image).resize(1200, 400).error(R.drawable.error).into(banner);
        }
        tvnome.setText(nome);
        //tv_dhini.setText(dini1 + ", " + hini + "h" + " -");
        //tv_dhfim.setText(" " + dfim1 + ", " + hfim + "h");
        String endcompleto = logradouro + " " + endereco + ", " + numero + ", " + bairro + "\n" + cidade + " - " + estado;
        tv_local.setText(endcompleto);


        //Mapa
        verMapa = (Button) findViewById(R.id.vmp);


        verMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DataLongOperationAsynchTask().execute();

            }
        });
    }


    private class readIngressos extends AsyncTask<Void, Void, Void> {
        SpotsDialog dialog = new SpotsDialog(EventosCIngressos.this, R.style.Custom);

        @Override
        protected void onPreExecute() {

            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... v) {


            ingressos();

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {

            if(dialog.isShowing()) {
                IngressoL.setAdapter(adapter);
                dialog.dismiss();
            }

        }

    }


    private String ingressos() {


        String resultado = null;

        try {
            RequestBody formBody = new FormBody.Builder()
                    .add("usuario", session.getId())
                    .add("id", idevento)
                    .add("flag", session.getFlag())
                    .build();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(session.getGlobalURL() + "/api/ingressos-evento/")
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            String json = response.body().string();
            Log.i("TESTECT", json);

            try {

                JSONObject responseFull = new JSONObject(json);
                JSONArray obj_array;

                // Aqui separo a mensagem de validação ("retorno":true);
                resultado = responseFull.getString("retorno");

                if (resultado.equals("true")) {


                    obj_array = responseFull.getJSONArray("ingressos");
                    ingressoCList.clear();
                    for (int i = 0; i < obj_array.length(); i++) {
                        JSONObject obj = obj_array.getJSONObject(i);
                        IngressoC iC = new IngressoC();
                        iC.setPed_ingre(obj.optString("ped_ingresso"));
                        iC.setIgressonome(obj.optString("ingressonome"));
                        iC.setNome(obj.optString("nome"));
                        iC.setQrcode(obj.optString("qrcode"));
                        iC.setBairro(obj.optString("bairro"));
                        iC.setData(obj.optString("data"));
                        iC.setCadeira(obj.optString("cadeira"));
                        iC.setCidade(obj.optString("cidade"));
                        iC.setEndereco(obj.optString("endereco"));
                        iC.setLogradouro(obj.optString("logradouro"));
                        iC.setNumero(obj.optString("numero"));
                        iC.setUf(obj.optString("uf"));
                        iC.setValor(obj.optString("valor"));
                        ingressoCList.add(iC);

                    }

                }

            } catch (JSONException e) {


                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);

            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private class DataLongOperationAsynchTask extends AsyncTask<String, Void, String[]> {
        ProgressDialog dialog = new ProgressDialog(EventosCIngressos.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Please wait...");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String[] doInBackground(String... params) {

            try {
                String response = getLatLongByURL("http://maps.google.com/maps/api/geocode/json?address=" + cidade + "," + estado + "&sensor=false").toLowerCase();
                // Log.d("response", "" + response);
                // Log.d("response", "http://maps.google.com/maps/api/geocode/json?address=" + num + "," + rua + end + "," + cidade + "," + estado + "&sensor=false");

                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");

                lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");

                Log.d("latitude", "" + lat);
                Log.d("longitude", "" + lng);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (dialog.isShowing()) {

                dialog.dismiss();

                String uri = String.format(Locale.ENGLISH, "geo:%f,%f?z=%d&q=%f,%f (%s)", lat, lng, 30, lat, lng, nome);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }

        }
    }


    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
