package br.com.konvita.konvita_clientes.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SyncPay extends AppCompatActivity {

    SessionPrefs session;
    int Autenti;
    String numcomp, status, cartao,
    paymentMethodId, cardToken, parcelas, cardIssuerId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_pay);

        session = new SessionPrefs(this);

        Bundle bundle = getIntent().getExtras();
        paymentMethodId = bundle.getString("cardmarc");
        cardToken = bundle.getString("token");
        parcelas = bundle.getString("parcelas");

        new ResultCar().execute();

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //Intent intent = new Intent(SyncPay.this, Home.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        //startActivity(intent);

    }


    private class ResultCar extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
        }


        @Override
        protected Void doInBackground(Void... v) {

            ResultCarrinho();


            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            if (Autenti == 1) {

                //startCongratsActivity();
                Intent intent = new Intent(SyncPay.this, FinalPagamentoMP.class);

                intent.putExtra("evento", session.getEvetemp());
                intent.putExtra("valortotal", "R$ " + session.getVlttemp().replace(".", ","));
                intent.putExtra("parcelas", parcelas);
                intent.putExtra("cartao", cartao);
                intent.putExtra("nc", numcomp);
                intent.putExtra("status", status);


                startActivity(intent);
                finish();
            }else if (Autenti == 2) {

                //startCongratsActivity();
                Intent intent = new Intent(SyncPay.this, FinalPagamentoMP.class);

                intent.putExtra("evento", session.getEvetemp());
                intent.putExtra("valortotal", "R$ " + session.getVlttemp().replace(".", ","));
                intent.putExtra("parcelas", parcelas);
                intent.putExtra("cartao", cartao);
                intent.putExtra("nc", numcomp);
                intent.putExtra("status", status);


                startActivity(intent);
                finish();
            }else if (Autenti == 3) {

                //startCongratsActivity();
                Intent intent = new Intent(SyncPay.this, FinalPagamentoMP.class);

                intent.putExtra("evento", session.getEvetemp());
                intent.putExtra("valortotal", "R$ " + session.getVlttemp().replace(".", ","));
                intent.putExtra("parcelas", parcelas);
                intent.putExtra("cartao", cartao);
                intent.putExtra("nc", numcomp);
                intent.putExtra("status", status);


                startActivity(intent);
                finish();
            } else if (Autenti == 4) {



            }
        }
    }


        private String ResultCarrinho() {


            String resultado = null;
            String responses = null;

            try {
                RequestBody formBody = new FormBody.Builder()
                        .add("valor", "R$ " + session.getVlttemp().replace(".", ","))
                        .add("token", cardToken)
                        .add("usuario", session.getId())
                        .add("paymentMethodId", paymentMethodId)
                        .add("categoria", session.getFlag())
                        .build();

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(session.getGlobalURL()+"/api/finalizar")
                        .post(formBody)
                        .build();

                Response response = client.newCall(request).execute();
                String json = response.body().string();
                Log.i("APX", json);

                try {

                    /*Log.i("APX", json);
                    Log.i("APX", session.getVlttemp());
                    Log.i("APX", cardToken);
                    Log.i("APX", session.getId());
                    Log.i("APX", paymentMethodId);*/
                    JSONObject responseFull = new JSONObject(json);
                    //JSONArray obj_array;

                   resultado = responseFull.getString("retorno");

                  if (resultado.equals("true")) {

                        responses = responseFull.getString("payment");
                        JSONObject result = new JSONObject(responses);

                        responses = result.getString("status");

                        if (responses.equals("201")) {

                            responses = result.getString("response");
                            JSONObject result2 = new JSONObject(responses);

                            status = result2.getString("status");
                            numcomp = result2.getString("collector_id");
                            status = result2.getString("status");
                            cartao = result2.getString("payment_method_id");
                            //status_detail

                            switch (status) {
                                case "approved":

                                    Autenti = 1;
                                    break;
                                case "in_process":

                                    Autenti = 2;
                                    break;
                                case "rejected":

                                    Autenti = 3;
                                    break;
                            }
                        }
                    }
                } catch (JSONException e) {
                    Autenti = 4;
                    Log.e("ERRO", "Não Encontrou Nenhum JSON", e);

                }


            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


}
