package br.com.konvita.konvita_clientes.model;



public class CarrinhoItem {

    private static CarrinhoItem instance;

    private  String sessaocarrinho, quantidade, ningresso, idevento, lote, vluni, nome;
    private  String nomeevento, vltotal, cadeira, max, imagem, vltotalpu, sessao;

    public CarrinhoItem() {}


    public static CarrinhoItem getInstance() {
        if (instance == null)
            instance = new CarrinhoItem();
        return instance;
    }

    public String getSessaocarrinho() {
        return sessaocarrinho;
    }

    public void setSessaocarrinho(String sessaocarrinho) {
        this.sessaocarrinho = sessaocarrinho;
    }

    public String getNomeevento() {
        return nomeevento;
    }

    public void setNomeevento(String nomeevento) {
        this.nomeevento = nomeevento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(String quantidade) {
        this.quantidade = quantidade;
    }

    public String getVluni() {
        return vluni;
    }

    public void setVluni(String vluni) {
        this.vluni = vluni;
    }

    public String getSessao() {
        return sessao;
    }

    public void setSessao(String sessao) {
        this.sessao = sessao;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public String getVltotalpu() {
        return vltotalpu;
    }

    public void setVltotalpu(String vltotalpu) {
        this.vltotalpu = vltotalpu;
    }

    public String getNingresso() {
        return ningresso;
    }

    public void setNingresso(String ningresso) {
        this.ningresso = ningresso;
    }

    public String getIdevento() {
        return idevento;
    }

    public void setIdevento(String idevento) {
        this.idevento = idevento;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getCadeira() {
        return cadeira;
    }

    public void setCadeira(String cadeira) {
        this.cadeira = cadeira;
    }
}






