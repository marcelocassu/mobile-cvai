package br.com.konvita.konvita_clientes.model;


public class SessaoFilme {

    private String sessao;
    private String nomefantasia, audio, sala, horario;
    private String dinicio, tela;


    public SessaoFilme(){

    }

    public String getSessao() {
        return sessao;
    }

    public void setSessao(String sessao) {
        this.sessao = sessao;
    }

    public String getNomefantasia() {
        return nomefantasia;
    }

    public void setNomefantasia(String nomefantasia) {
        this.nomefantasia = nomefantasia;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getDinicio() {
        return dinicio;
    }

    public void setDinicio (String dinicio) {
        this.dinicio = dinicio;
    }

    public String getTela() {
        return tela;
    }

    public void setTela(String tela) {
        this.tela = tela;
    }



}
