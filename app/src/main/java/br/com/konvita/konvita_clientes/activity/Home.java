package br.com.konvita.konvita_clientes.activity;


import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.db.dbController;
import br.com.konvita.konvita_clientes.db.dbo;
import br.com.konvita.konvita_clientes.fragment.cinema;
import br.com.konvita.konvita_clientes.fragment.eventosProximos;
import br.com.konvita.konvita_clientes.model.CarrinhoItem;
import br.com.konvita.konvita_clientes.utils.Utils;
import br.com.konvita.konvita_clientes.fragment.eventosComprados;
import br.com.konvita.konvita_clientes.fragment.eventosDisponiveis;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static br.com.konvita.konvita_clientes.R.layout.activity_main;


public class Home extends AppCompatActivity {
    CoordinatorLayout coordinatorLayout;
    String infojsonData, auth = "0";
    int badge;
    SectionsPagerAdapter mSectionsPagerAdapter;
    private SessionPrefs session;
    private HashMap<Integer, Fragment> fragmentosUtilizados = new HashMap<>();
    CarrinhoItem cI = new CarrinhoItem();
    SQLiteDatabase db;
    dbController crud = new dbController(this);
    dbo dbo;


    @Override
    protected void onStart() {
        super.onStart();

        if (session.getMPFinal().equals("0")) {
            //Do Nothing
        } else if (session.getMPFinal().equals("1")) {
            new GeraCarrinho().execute();
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_main);


        session = new SessionPrefs(this);

        session.setMPFinal("1");
        if (!session.loggedin()) {
            logout();
        }


        /*----------------------------------
                 DB Inicio
        ----------------------------------*/
        db = openOrCreateDatabase("IngressoDB.db", Context.MODE_PRIVATE, null);
        dbo = new dbo(this);
        dbo.onCreate(db);
        session.setAuth("1");
        crud.deletatudo();
        db.close();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Olá, " + session.getNome() + " " + session.getSobrenome());
        setSupportActionBar(toolbar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_content);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }


    private void logout() {
        session.setLoggedin(false);
        session.setNome(null);
        session.setSobrenome(null);
        crud.deletatudo();
        finish();
        startActivity(new Intent(Home.this, Login.class));
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.item_cart);

        // Obtener drawable del item
        LayerDrawable icon = (LayerDrawable) item.getIcon();


        // Actualizar el contador
        Utils.setBadgeCount(Home.this, icon, badge);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.item_sobre) {
            Intent intent = new Intent(getApplicationContext(), Sobre.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        if (id == R.id.item_perfil) {
            Intent intent = new Intent(getApplicationContext(), PerfilUsuario.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        if (id == R.id.item_cart) {

            new GeraCarrinho().execute();

            if (auth.equals("1")) {
                Intent intent = new Intent(getApplicationContext(), Carrinho.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                startActivity(intent);
            } else if (auth.equals("0")) {
                Snackbar snackbar = Snackbar.make(coordinatorLayout, "Carrinho Vazio", Snackbar.LENGTH_LONG);
                View view = snackbar.getView();
                view.setBackgroundColor(Color.rgb(219, 22, 22));
                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextSize(20);
                tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                snackbar.show();
            }


        }
        if (id == R.id.item_sair) {
            new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setCustomImage(R.drawable.cvai_textlogo)
                    .setTitleText("Deseja se deslogar?")
                    .setConfirmText("Sim")
                    .setCancelText("Não")

                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            logout();
                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    })

                    .show();
        }

        return super.onOptionsItemSelected(item);
    }

    /*----------------------------------
                     Fragment
      ----------------------------------*/
    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        private SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new eventosDisponiveis();
                    fragmentosUtilizados.put(position, fragment);
                    break;

                case 1:
                    fragment = new cinema();
                    fragmentosUtilizados.put(position, fragment);
                    break;
                case 2:
                    fragment = new eventosComprados();
                    fragmentosUtilizados.put(position, fragment);
                    break;
                case 3:
                    fragment = new eventosProximos();
                    fragmentosUtilizados.put(position, fragment);
                    break;

            }
            return fragment;
        }


        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Eventos";
                case 1:
                    return "Cinema";
                case 2:
                    return "Meus Eventos";
                case 3:
                    return "Eventos Proximos";
            }
            return null;
        }

    }

    /*----------------------------------
            Verifica Carrinho Vazio
        ----------------------------------*/
    private class GeraCarrinho extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... v) {

            try {

                session = new SessionPrefs(Home.this);
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(session.getGlobalURL() + "/api/meu-carrinho/" + session.getId() + "/").build(); // ENVIANDO DADOS PARA A URL ;
                Response responses = null;

                try {

                    responses = client.newCall(request).execute();
                    infojsonData = responses.body().string();


                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {

                    JSONObject responseFull = new JSONObject(infojsonData);
                    JSONArray obj_array;
                    String resultado = responseFull.getString("retorno");

                    if (resultado.equals("true")) {

                        obj_array = responseFull.getJSONArray("MeuCarrinho");

                        for (int i = 0; i < obj_array.length(); i++) {
                            JSONObject obj = obj_array.getJSONObject(i);
                            //cI.setSessaocarrinho(obj.optString("sessaocarrinho"));
                            cI.setQuantidade(obj.optString("quantidade"));
                            //cI.setVltotal(obj.optString("vltotal"));
                            //cI.setNomeevento(obj.optString("nomeevento"));
                            //cI.setImagem(obj.optString("imagem"));
                        }
                        auth = "1";

                    } else {
                        auth = "0";
                    }


                } catch (JSONException e) {
                    Log.e("ERRO", "Não Encontrou Nenhum JSON", e);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {


            if (auth.equals("1")) {

                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "Carrinho com Itens", Snackbar.LENGTH_SHORT);
                View view = snackbar.getView();
                view.setBackgroundColor(Color.rgb(8, 208, 35));
                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                //tv.setTypeface(tv.getTypeface(), Typeface.BOLD);
                //tv.setMaxLines(3);
                tv.setTextSize(20);
                tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                snackbar.show();

            } else if (auth.equals("0")) {
                badge = 0;

            }


        }

    }

}
