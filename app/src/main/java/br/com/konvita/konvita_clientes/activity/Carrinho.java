package br.com.konvita.konvita_clientes.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.mercadopago.constants.PaymentMethods;
import com.mercadopago.constants.PaymentTypes;
import com.mercadopago.core.MercadoPagoCheckout;
import com.mercadopago.model.Item;
import com.mercadopago.model.PaymentData;
import com.mercadopago.preferences.CheckoutPreference;
import com.mercadopago.preferences.DecorationPreference;
import com.mercadopago.util.JsonUtil;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.adapter.carrinhoAdapter;
import br.com.konvita.konvita_clientes.adapter.carrinhoCinemaAdapter;
import br.com.konvita.konvita_clientes.model.CarrinhoItem;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import dmax.dialog.SpotsDialog;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.mercadopago.constants.Sites.BRASIL;


public class Carrinho extends AppCompatActivity {


    ListView Carrinho;
    carrinhoAdapter Adapter;
    carrinhoCinemaAdapter AdapterCine;
    List<CarrinhoItem> carrinhoMList = new ArrayList<CarrinhoItem>();
    Button finalbuy, backbuy;
    SessionPrefs session;
    TextView valorfull, eventoname;
    ImageView avatar;
    SpotsDialog dialog;
    String jsonData, paymentMethodId, cardToken, auth, image;



    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.carrinho);


        dialog = new SpotsDialog(Carrinho.this, R.style.Custom);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();


        session = new SessionPrefs(this);

        valorfull = (TextView) findViewById(R.id.valorfullcarrinho);
        backbuy = (Button) findViewById(R.id.backbuy);
        finalbuy = (Button) findViewById(R.id.finalbuy);
        avatar = (ImageView) findViewById(R.id.avatarcarrinho);
        eventoname = (TextView) findViewById(R.id.eventoname);


        //ListView
        Carrinho = (ListView) findViewById(R.id.listaCarrinho);
        //Adapter custom
        Adapter = new carrinhoAdapter(this, carrinhoMList);
        AdapterCine = new carrinhoCinemaAdapter(this, carrinhoMList);
        //Executa a função
        new carregacarrinho().execute();

        //Image para checkout
        if(session.getFlag().equals("0")){
            image = session.getGlobalURL() + "/imagens/bannereventos/" + session.getFotoevetemp();
        }else if (session.getFlag().equals("1")){
            image = session.getGlobalURL() + "/imagens/bannerfilmes/" + session.getFotoevetemp();
        }

        backbuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new RemoveC().execute();
                //onBackPressed();
                Intent intent = new Intent(getApplicationContext(), Home.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });
        finalbuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CheckoutPreference checkoutPreference = new CheckoutPreference.Builder()


                        .addItem(new Item(session.getEvetemp(), new BigDecimal(session.getVlttemp()), (image)))
                        .setSite(BRASIL)
                        .setPayerEmail(session.getEmail())
                        .addExcludedPaymentType(PaymentTypes.TICKET) //Handle exclusions by payment types
                        .addExcludedPaymentMethod(PaymentMethods.BRASIL.BOLBRADESCO) //Exclude specific payment methods
                        .setMaxInstallments(1) //Limit the amount of installments
                        .build();
                startMercadoPagoCheckout(checkoutPreference);
            }
        });
    }

    private void startMercadoPagoCheckout(CheckoutPreference checkoutPreference) {

        DecorationPreference decorationPreference = new DecorationPreference.Builder()
                .setBaseColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
                .build();

        new MercadoPagoCheckout.Builder()
                .setActivity(this)
                .setPublicKey("TEST-92dcff18-5c49-4241-a68e-aa5d00b2cd09")
                .setCheckoutPreference(checkoutPreference)
                .setDecorationPreference(decorationPreference)
                .startForPaymentData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MercadoPagoCheckout.CHECKOUT_REQUEST_CODE) {
            if (resultCode == MercadoPagoCheckout.PAYMENT_DATA_RESULT_CODE) {
                PaymentData paymentData = JsonUtil.getInstance().fromJson(data.getStringExtra("paymentData"), PaymentData.class);

                //Done!
                paymentMethodId = paymentData.getPaymentMethod().getId();
                Long cardIssuerId = paymentData.getIssuer() == null ? null : paymentData.getIssuer().getId();
                Integer installment = paymentData.getPayerCost() == null ? null : paymentData.getPayerCost().getInstallments();
                cardToken = paymentData.getToken() == null ? null : paymentData.getToken().getId();
                Long campaignId = paymentData.getDiscount() == null ? null : paymentData.getDiscount().getId();

                Intent intent = new Intent(Carrinho.this, SyncPay.class);
                intent.putExtra("cardmarc", paymentMethodId);
                intent.putExtra("token", cardToken);
                intent.putExtra("parcelas", installment.toString());
                startActivity(intent);
                finish();

                Log.i("TOKEN", cardToken);
                Log.i("issuer", cardIssuerId.toString());
                Log.i("paymentMethod", paymentMethodId);
                Log.i("paymentMethod", installment.toString());

            } else if (resultCode == RESULT_CANCELED) {
                if (data != null && data.getStringExtra("mercadoPagoError") != null) {
                    //Resolve error in checkout
                } else {
                    //Resolve canceled checkout
                }
            }


        }
    }


    private class carregacarrinho extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... v) {
            carrinhoItem();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if(auth.equals("0")){
                Carrinho.setAdapter(Adapter);
                valorfull.setText("R$ " + session.getVlttemp());
                eventoname.setText(session.getEvetemp());
                Picasso.with(Carrinho.this).load(session.getGlobalURL() + "/imagens/bannereventos/" + session.getFotoevetemp())
                        .error(R.drawable.ic_visibility_off)
                        .into(avatar);


            }else if(auth.equals("1")){
                Carrinho.setAdapter(AdapterCine);
                valorfull.setText("R$ " + session.getVlttemp());
                eventoname.setText(session.getEvetemp());
                Picasso.with(Carrinho.this).load(session.getGlobalURL() + "/imagens/bannerfilmes/" + session.getFotoevetemp())
                        .error(R.drawable.ic_visibility_off)
                        .into(avatar);

            }


            if (dialog.isShowing()) {
                dialog.dismiss();
            }

        }

    }


    public void carrinhoItem() {


        try {

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(session.getGlobalURL() + "/api/meu-carrinho/" + session.getId() + "").build(); // ENVIANDO DADOS PARA A URL ;
            Response responses = null;

            try {

                responses = client.newCall(request).execute();
                jsonData = responses.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }


            try {


                // Aqui pega o retorno ({"retorno":true,"msg":[{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}]});
                JSONObject responseFull = new JSONObject(jsonData);
                //Log.i("APX", jsonData);
                JSONArray obj_array;

                // Aqui separo a mensagem de validação ("retorno":true);
                String resultado = responseFull.getString("retorno");

                // Se for true pega dados dentro do jsonArray "EventosUsuario" - ("usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}])
                carrinhoMList.clear();
                if (resultado.equals("true")) {

                    obj_array = responseFull.getJSONArray("MeuCarrinho");
                    JSONObject obj1 = obj_array.getJSONObject(0);
                    CarrinhoItem cI = new CarrinhoItem();
                    cI.setSessao(obj1.optString("sessao"));
                    Log.i("APX", cI.getSessao());

                  if(cI.getSessao().equals("0")){
                      auth = "0";
                    for (int i = 0; i < obj_array.length(); i++) {
                        JSONObject obj = obj_array.getJSONObject(i);
                        cI = new CarrinhoItem();
                        cI.setSessaocarrinho(obj.optString("sessaocarrinho"));
                        cI.setNome(obj.optString("nome"));
                        cI.setNingresso(obj.optString("ingresso"));
                        cI.setIdevento(obj.optString("evento"));
                        cI.setLote(obj.optString("lote"));
                        session.setEvetemp(obj.optString("nomeevento"));
                        session.setFotoevetemp(obj.getString("imagem"));
                        session.setVlttemp(obj.optString("vltotal"));
                        cI.setQuantidade(obj.optString("quantidade"));
                        cI.setVluni(obj.optString("precoUnitario"));
                        cI.setVltotalpu(obj.optString("valorTotal"));
                        cI.setMax(obj.optString("max"));
                        carrinhoMList.add(cI);
                    }
                    Log.i("APX", carrinhoMList.toString());

                  }else {
                      auth = "1";

                      JSONObject obj2 = obj_array.getJSONObject(0);
                      cI = new CarrinhoItem();
                      cI.setSessaocarrinho(obj2.optString("sessaocarrinho"));
                      session.setEvetemp(obj2.optString("nome"));
                      session.setFotoevetemp(obj2.getString("imagem"));
                      session.setVlttemp(obj2.optString("vltotal"));
                      cI.setQuantidade(obj2.optString("quantidade"));
                      cI.setVluni(obj2.optString("precoUnitario"));
                      cI.setNome(obj2.optString("nome"));


                      for (int i = 0; i < obj_array.length(); i++) {
                          JSONObject obj = obj_array.getJSONObject(i);
                          cI = new CarrinhoItem();
                          cI.setCadeira(obj.optString("cadeira"));
                          carrinhoMList.add(cI);
                         // Log.i("APX", cI.getCadeira());
                      }

                  }

                } else {
                    Log.i("REX", "ERRO");
                }


            } catch (JSONException e) {
                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private class RemoveC extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... v) {
            Removeitem();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {


        }

    }

    public void Removeitem() {


        try {

            OkHttpClient client = new OkHttpClient();
            //Request request = new Request.Builder().url("http://konvita.kplay.com.br/api/exclui-item-carrinho/" + carrinhoItem.getSessaocarrinho() + "").build(); // ENVIANDO DADOS PARA A URL ;
            Request request = new Request.Builder().url(session.getGlobalURL() + "/api/limpar-carrinho/" + session.getId() + "").build(); // ENVIANDO DADOS PARA A URL ;
            Response responses = null;

            try {

                responses = client.newCall(request).execute();
                jsonData = responses.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }


            try {


                // Aqui pega o retorno ({"retorno":true,"msg":[{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}]});
                JSONObject responseFull = new JSONObject(jsonData);
                //JSONArray obj_array ;

                // Aqui separo a mensagem de validação ("retorno":true);
                String resultado = responseFull.getString("retorno");

                // Se for true pega dados dentro do jsonArray "EventosUsuario" - ("usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}])
                if (resultado.equals("true")) {
                    session.setEvetemp("");
                    session.setFotoevetemp("");
                    session.setVlttemp("");

                } else {


                }

            } catch (JSONException e) {
                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



}
