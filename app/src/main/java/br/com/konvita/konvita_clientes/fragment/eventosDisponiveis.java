package br.com.konvita.konvita_clientes.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.adapter.eventosAdapterDisponiveis;
import br.com.konvita.konvita_clientes.db.dbController;
import br.com.konvita.konvita_clientes.model.EventoD;
import br.com.konvita.konvita_clientes.model.Ingresso;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;



public class eventosDisponiveis extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    SearchView sv;
    ListView Eventos;
    SweetAlertDialog pDialog;
    Ingresso ingresso;
    List<EventoD> eventosList = new ArrayList<EventoD>();
    List<Ingresso> ingressoList = new ArrayList<Ingresso>();
    SwipeRefreshLayout swipeLayout2;
    eventosAdapterDisponiveis adapter1;
    SessionPrefs session;



    public eventosDisponiveis() {
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        session = new SessionPrefs(getContext());
        //ListView
        Eventos = (ListView) getActivity().findViewById(R.id.listView01);
        //Adapter custom
        adapter1 = new eventosAdapterDisponiveis(getActivity(), eventosList);

        swipeLayout2 = (SwipeRefreshLayout) getActivity().findViewById(R.id.Swipecontainer1);
        swipeLayout2.setOnRefreshListener(this);
        swipeLayout2.setRefreshing(true);





        sv = (SearchView) getActivity().findViewById(R.id.seacher);
       sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String arg0) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // TODO Auto-generated method stub

                adapter1.getFilter().filter(query);

                return false;
            }
        });

        //Receber o Json
        new readlocalJson().execute();

    }

    private class readlocalJson extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... strings) {
            String jsonData = null;
            try {

                OkHttpClient client = new OkHttpClient();
                OkHttpClient clientWith30sTimeout = client.newBuilder().readTimeout(120, TimeUnit.SECONDS).build();
                Request request = new Request.Builder().url(session.getGlobalURL()+"/api/eventos").build();
                Response responses = null;

                try {

                    responses = clientWith30sTimeout.newCall(request).execute();
                    jsonData = responses.body().string();


                } catch (IOException e) {
                    e.printStackTrace();
                }


                try {

                    //para exibir.
                    JSONObject responseFull = new JSONObject(jsonData);
                    JSONArray obj_array = null;
                    obj_array = responseFull.getJSONArray("eventos");
                    eventosList.clear();
                    for (int i = 0; i < obj_array.length(); i++) {
                        JSONObject obj = obj_array.getJSONObject(i);
                        EventoD evento = new EventoD();
                        evento.setNome(obj.optString("nome"));
                        evento.setDescricao(obj.optString("descricao"));
                        evento.setCategoria(obj.optString("categoria"));
                        evento.setImage(obj.getString("imagem"));
                        evento.setDhini(obj.getString("dhinicio"));
                        evento.setDhfim(obj.getString("dhfim"));
                        evento.setDhinicheck(obj.getString("dhinicheck"));
                        evento.setDhfimcheck(obj.getString("dhfimcheck"));
                        evento.setLogradouro(obj.getString("logradouro"));
                        evento.setEndereco(obj.getString("endereco"));
                        evento.setNumero(obj.getString("numero"));
                        evento.setLocal(obj.getString("pontoReferencia"));
                        evento.setCidade(obj.getString("cidade"));
                        evento.setUf(obj.getString("uf"));
                        evento.setIdevento(obj.getString("evento"));

                        // Log.i("REX", evento.getIdevento());
                        // Log.i("REX", evento.getNome());

                        //Log.i("REX", obj.optString("ingressos"));
                        //JSONObject result = new JSONObject(resultado);
                        //Log.i("REX", result.toString());


                        JSONArray obj_array2 = null;
                        obj_array2 = obj.optJSONArray("ingressos");
                        if (!obj.optString("ingressos").isEmpty()) {

                            for (int j = 0; j < obj_array2.length(); j++) {

                                JSONObject obj2 = obj_array2.getJSONObject(j);
                                ingresso = Ingresso.getInstance();
                                ingresso.setIdevento(obj2.optString("evento"));
                                ingresso.setNomeEve(obj2.optString("nomeEvento"));
                                ingresso.setCtgIngresso(obj2.optString("nomeIngresso"));
                                ingresso.setnIngresso(obj2.getString("ingresso"));
                                ingresso.setNumLote(obj2.getString("lote"));
                                ingresso.setPreco(obj2.optString("preco"));
                                ingresso.setTipoIngresso(obj2.optString("tipoIngresso"));
                                ingresso.setQtd(obj2.optString("quantidade"));
                                ingressoList.add(ingresso);

                                if(session.getAuth().equals("1")) {
                                    //Insere no DB todos os ingressos
                                    dbController crud = new dbController(getActivity());
                                    crud.insereDado(ingresso.getIdevento(), ingresso.getNomeEve(), ingresso.getnIngresso(), ingresso.getCtgIngresso(),
                                            ingresso.getTipoIngresso(), ingresso.getNumLote(), ingresso.getQtd(), ingresso.getPreco());
                                }
                            }
                        } else {

                            Log.i("REX", "não tem nada");

                        }

                        eventosList.add(evento);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(final String s) {
            super.onPostExecute(s);
            Eventos.setAdapter(adapter1);
            swipeLayout2.setRefreshing(false);
            session.setAuth("0");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.eventos_disponiveis, container, false);


    }


    @Override
    public void onRefresh() {
        new readlocalJson().execute();
        dbController crud = new dbController(getActivity());
        session.setAuth("1");
        crud.deletatudo();

    }

}


