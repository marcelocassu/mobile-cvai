package br.com.konvita.konvita_clientes.activity;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.model.SessionPrefs;


public class PerfilUsuario extends AppCompatActivity {

    TextView nomepessoa,
            genero, datenascimento,
            cpf, tel;
    SessionPrefs sessionPrefs;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.usuario_perfil);

        //Sessão que recupera infos do usuario
        sessionPrefs = new SessionPrefs(this);

        //
        ImageView avatar = (ImageView) findViewById(R.id.avatar);
        nomepessoa = (TextView) findViewById(R.id.nomepessoa);
        genero = (TextView) findViewById(R.id.genero);
        datenascimento = (TextView) findViewById(R.id.aniversario);
        cpf = (TextView) findViewById(R.id.cpf);
        tel = (TextView) findViewById(R.id.celular);

        String M = "Masculino";
        String F = "Feminino";
        String telcompleto = "(" + sessionPrefs.getDdd() + ") " + sessionPrefs.getCelular();
        String avatarK = sessionPrefs.getGlobalURL()+"/imagens/fotoperfil/"+sessionPrefs.getFotoperfil();

        datenascimento.setText(sessionPrefs.getDt());
        nomepessoa.setText(sessionPrefs.getNome());
        if (sessionPrefs.getGenero().equals("M")) {
            genero.setText(M);
        } else if (sessionPrefs.getGenero().equals("F")) {
            genero.setText(F);
        }
        cpf.setText(sessionPrefs.getCpf());
        tel.setText(telcompleto);
        Picasso.with(this).load(avatarK).resize(200 , 200).centerCrop().error(R.drawable.personalprofile).into(avatar);
    }

}
