package br.com.konvita.konvita_clientes.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.adapter.eventosAdapterDisponiveis;
import br.com.konvita.konvita_clientes.adapter.eventosAdapterProximo;
import br.com.konvita.konvita_clientes.db.dbController;
import br.com.konvita.konvita_clientes.model.EventoD;
import br.com.konvita.konvita_clientes.model.EventoP;
import br.com.konvita.konvita_clientes.model.Ingresso;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class eventosProximos extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    SearchView sv;
    ListView Eventos;
    SweetAlertDialog pDialog;
    Ingresso ingresso;
    List<EventoP> eventosList = new ArrayList<EventoP>();
    SwipeRefreshLayout swipeLayout4;
    eventosAdapterProximo adapter;
    SessionPrefs session;

    public eventosProximos() {    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Session
        session = new SessionPrefs(getContext());

        //ListView
        Eventos = (ListView) getActivity().findViewById(R.id.listView03);

        //Adapter custom
        adapter = new eventosAdapterProximo(getActivity(), eventosList);

        //Async
        new AsyncLocais().execute();

        //Refresh
        swipeLayout4 = (SwipeRefreshLayout) getActivity().findViewById(R.id.Swipecontainer4);
        swipeLayout4.setOnRefreshListener(this);
        swipeLayout4.setRefreshing(true);

        //Search
        sv = (SearchView) getActivity().findViewById(R.id.seacher4);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String arg0) {

                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                // TODO Auto-generated method stub
                adapter.getFilter().filter(query);
                return false;
            }
        });
    }

    private class AsyncLocais extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... strings) {

            eventoslocais();

            return null;
        }



        @Override
        protected void onPostExecute(final String s) {
            super.onPostExecute(s);
            Eventos.setAdapter(adapter);
            swipeLayout4.setRefreshing(false);


        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.eventos_proximos, container, false);
    }

    public String eventoslocais() {


        String resultado = null;

        try {
            RequestBody formBody = new FormBody.Builder()
                    .add("cidade", "Piraju")
                    .build();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(session.getGlobalURL()+"/api/locais-evento")
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            String json = response.body().string();
            Log.i("Proximos", json);

            try {

                // Aqui pega o retorno ({"retorno":true,"msg":[{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}]});
                JSONObject responseFull = new JSONObject(json);

                // Aqui separo a mensagem de validação ("retorno":true);
                resultado = responseFull.getString("retorno");

                // Se for true pega dados dentro do jsonArray "EventosUsuario" - ("usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}])
                if (resultado.equals("true")) {

                   JSONArray obj_array = null;
                    obj_array = responseFull.getJSONArray("eventos");

                    for (int i = 0; i < obj_array.length(); i++) {
                        JSONObject obj = obj_array.getJSONObject(i);
                        EventoP ep = new EventoP();
                        ep.setNome(obj.optString("nome"));
                        ep.setDescricao(obj.optString("descricao"));
                        ep.setCategoria(obj.optString("categoria"));
                        ep.setImage(obj.optString("imagem"));
                        ep.setDini(obj.optString("inicio"));
                        ep.setDfim(obj.optString("dhfim"));
                        ep.setLogradouro(obj.optString("logradouro"));
                        ep.setEndereco(obj.optString("endereco"));
                        ep.setNumero(obj.optString("numero"));
                        ep.setLocal(obj.optString("pontoReferencia"));
                        ep.setCidade(obj.optString("cidade"));
                        ep.setUf(obj.optString("uf"));
                        ep.setIdevento(obj.optString("evento"));
                        eventosList.add(ep);

                    }
                }

            } catch (JSONException e) {

                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);

            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public void onRefresh() {
        new AsyncLocais().execute();
        //dbController crud = new dbController(getActivity());
        //crud.deletatudo();

    }

}
