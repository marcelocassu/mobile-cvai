package br.com.konvita.konvita_clientes.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.icu.math.BigDecimal;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mercadopago.core.MercadoPago;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.activity.Carrinho;
import br.com.konvita.konvita_clientes.activity.Evento;
import br.com.konvita.konvita_clientes.model.CarrinhoItem;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import dmax.dialog.SpotsDialog;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static br.com.konvita.konvita_clientes.R.layout.carrinho_row;


public class carrinhoAdapter extends BaseAdapter {

    private Activity activity;
    private String soma = null;
    private String addoremove = null;
    private List<CarrinhoItem> carrinhoList;
    private Context context;


    public carrinhoAdapter(Context c, List<CarrinhoItem> carrinhoItem) {
        this.carrinhoList = carrinhoItem;
        this.context = c;
        activity = (Activity) context;
    }

    @Override
    public int getCount() {
        return carrinhoList.size();
    }

    @Override
    public Object getItem(int position) {
        return carrinhoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return carrinhoList.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        ViewHolder viewHolder = null;


        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(carrinho_row, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        final SessionPrefs session = new SessionPrefs(context);
        final CarrinhoItem currentItem = carrinhoList.get(position);

        //Ativa desincronizadamente a adição no carrinho
        final ViewHolder finalViewHolder = viewHolder;

        class addOrRemove extends AsyncTask<Void, Void, Void> {
            SpotsDialog dialog = new SpotsDialog(context, R.style.Custom);

            @Override
            protected void onPreExecute() {
                dialog.setCancelable(false);
                dialog.show();
                finalViewHolder.add.setVisibility(View.GONE);
                finalViewHolder.remove.setVisibility(View.GONE);

            }

            @Override
            protected Void doInBackground(Void... v) {
                try {
                    RequestBody formBody = new FormBody.Builder()
                            .add("ingressos", currentItem.getNingresso())
                            .add("idevento", currentItem.getIdevento())
                            .add("usuario", String.valueOf(session.getId()))
                            //.add("frete", "0.00")
                            //.add("valordesconto", "0.00")
                            .add("nome", currentItem.getNome())
                            .add("lote", currentItem.getLote())
                            .add("quantidade", soma)
                            .add("valor", currentItem.getVluni())
                            .build();

                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(session.getGlobalURL()+"/api/add-carrinho")
                            .post(formBody)
                            .build();

                    Response response = client.newCall(request).execute();
                    /* String json = response.body().string();
                    Log.i("APX", json);
                    Log.i("APX", "QUANTIDADE ATUAL: "+currentItem.getQuantidade());
                    Log.i("APX", "MAXIMO: "+currentItem.getMax());
                    Log.i("APX", "QUANTIDADE ADICIONADA: "+soma);*/


                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
                @Override
                protected void onPostExecute (Void result){

                    if (addoremove.equals("1")) {
                        Intent intent = new Intent(context, Carrinho.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                    }else if (addoremove.equals("0")){
                        Intent intent = new Intent(context, Carrinho.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_righ);

                    }
                    if (dialog.isShowing()){
                        dialog.dismiss();
                    }
              }
        }


        viewHolder.itemName.setText(currentItem.getNome());
        viewHolder.valorUnitarioIngresso.setText("R$ "+currentItem.getVluni());
        viewHolder.qtde.setText(currentItem.getQuantidade());
        viewHolder.valortotalingresso.setText("R$ "+currentItem.getVltotalpu());
        viewHolder.quantidademaxuni.setText(currentItem.getMax()+" Ingressos");

     viewHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               if(currentItem.getQuantidade().equals("1")){
                        Toast.makeText(context, "Você precisa ter pelo menos um convite", Toast.LENGTH_LONG).show();
                    }else {
                        new addOrRemove().execute();
                        addoremove ="0";
                        soma = "-1";
                    }

            }
        });

        viewHolder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(currentItem.getQuantidade().equals(currentItem.getMax())){
                    Toast.makeText(context, "Você não pode adicionar mais convites", Toast.LENGTH_LONG).show();

                }else{
                    new addOrRemove().execute();
                    addoremove = "1";
                    soma = "1";
                }

            }
        });

        return convertView;
    }


    //ViewHolder inner class
    private class ViewHolder {
        TextView itemName;
        TextView valorUnitarioIngresso;
        TextView valortotalingresso;
        TextView qtde;
        TextView quantidademaxuni;
        Button remove, add;
        private ViewHolder(View view) {

            itemName = (TextView) view.findViewById(R.id.usuariocarrinho);
            valorUnitarioIngresso = (TextView) view.findViewById(R.id.valor_unitario);
            qtde = (TextView) view.findViewById(R.id.qtd_ingresso);
            valortotalingresso = (TextView) view.findViewById(R.id.valor_total_ingresso);
            quantidademaxuni = (TextView) view.findViewById(R.id.quantidademaxuni);
            add = (Button)view.findViewById(R.id.add);
            remove = (Button)view.findViewById(R.id.remove);

        }
    }
}

