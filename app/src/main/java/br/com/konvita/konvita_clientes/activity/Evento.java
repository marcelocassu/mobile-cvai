package br.com.konvita.konvita_clientes.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.DialogPreference;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.fragment.eventosDisponiveis;
import br.com.konvita.konvita_clientes.model.CarrinhoItem;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Evento extends AppCompatActivity {

    Toolbar toolbar;
    TextView tvnome;
    TextView tvdescricao;
    TextView tvKplay;
    TextView tv_dhini;
    TextView tv_local;
    TextView valor;
    Button bt_addC, verMapa, ing;
    double lat, lng;
    String nome, acessorio, url,
            image, valoringresso,
            idingresso, idevento,
            lote, tipoingre, cidade,
            estado, rua, end, endereco, num, auth = "0";
    Response response = null;
    SessionPrefs session;
    SQLiteDatabase db;
    String json, Auth = "1";
    SweetAlertDialog pDialog;


    //Lista de infos mandadas para o carrinho
    List<String> ingrelist = new ArrayList<String>();
    ArrayList<String> tipolist = new ArrayList<String>();
    ArrayList<String> tipolist2 = new ArrayList<String>();
    List<String> precolist = new ArrayList<String>();
    List<String> lotelist = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.activity_evento);
        } else {
            setContentView(R.layout.activity_evento);
        }


        session = new SessionPrefs(this);
        session.setFlag("0");
        tvnome = (TextView) findViewById(R.id.tvevento2);
        tvdescricao = (TextView) findViewById(R.id.tvevento3);
        ImageView banner = (ImageView) findViewById(R.id.banner);
        toolbar = (Toolbar) findViewById(R.id.tb_evento);
        bt_addC = (Button) findViewById(R.id.bt_evento);
        verMapa = (Button) findViewById(R.id.vmp);
        ing = (Button) findViewById(R.id.ing);
        tvKplay = (TextView) findViewById(R.id.tvKplay);
        tv_dhini = (TextView) findViewById(R.id.dhini);
        tv_local = (TextView) findViewById(R.id.local);
        valor = (TextView) findViewById(R.id.valor);


        //Recebe os dados do Fragment EventoD
        Bundle bundle = getIntent().getExtras();
        nome = bundle.getString("nome");
        String categoria = bundle.getString("descricao");
        image = bundle.getString("imagem");

        //Localização
        String local = bundle.getString("local");
        rua = bundle.getString("logradouro");
        end = bundle.getString("endereco").replaceAll(" ", "");
        endereco = bundle.getString("endereco");
        num = bundle.getString("num");

        //Rua//endereco//numero
        cidade = bundle.getString("cidade");
        estado = bundle.getString("uf");

        //Data e hora
        String dhini1 = bundle.getString("dhini");
        String dhfim1 = bundle.getString("dhfim");
        String dhinicheck = bundle.getString("inicheck");
        String hfim = bundle.getString("fimcheck");
        acessorio = session.getGlobalURL()+"/imagens/bannereventos/" + image;

        //URL Button
        idevento = bundle.getString("idurl");
        url = session.getGlobalURL()+"/detalhes-eventos/" + idevento;

        //Seta as variaveis com seus devidos valores
        Picasso.with(Evento.this).load(acessorio).resize(1200, 400).error(R.drawable.error).into(banner);
        tvnome.setText(nome);
        tvdescricao.setText("Descrição do evento: " + "\n" + categoria);
        tv_dhini.setText("Início - "+dhini1 + " h");
        tv_local.setText(rua+" "+endereco+", "+num+ " - " + cidade + ", " + estado);



 /*----------------------------------
               SPINNER
    ----------------------------------*/
        // criando o Array de String para encher o Spinner
        //ArrayAdapter<String> aOpcoes;
        //Cria Spinner
        //Spinner spinner = (Spinner) findViewById(R.id.spinner);
        //Chamando o Banco
        db = openOrCreateDatabase("IngressoDB.db", Context.MODE_PRIVATE, null);
        //Iniciando cursor pra trazer infos
        Cursor c = db.rawQuery("SELECT * FROM Ingresso where Evento = " + idevento + "", null);
        //Verifica se está vazio
        if (c.getCount() == 0) {

            //Toast.makeText(this, "Dados gravados sem sucesso", Toast.LENGTH_SHORT).show();
            c.close();

        } else {

            while (c.moveToNext()) {
                ingrelist.add(c.getString(2));
                tipolist.add(c.getString(3) + " / R$ "+c.getString(7));
                tipolist2.add(c.getString(3));
                precolist.add(c.getString(7));
                lotelist.add(c.getString(5));
            }
            //Log.i("APX", tipolist.toString());
            c.close();
        }


        /*aOpcoes = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tipolist);
        spinner.setAdapter(aOpcoes);

        //ArrayAdapter adapter = ArrayAdapter.createFromResource(this, tipolist, R.layout.spinner_item);
        //spinner.setAdapter(adapter);

        //valor.setText(precolist.get(0));


        //Recupera o clique com seleção no spinner e altera as infos
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //Valor do ingresso pro cliente
                valor.setText(precolist.get(position));
                //seta as variaveis que vão para o carrinho
                valoringresso = precolist.get(position);
                idingresso = ingrelist.get(position);
                tipoingre = tipolist.get(position);
                lote = lotelist.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

         /*----------------------------------
                    Alert Dialog
          ----------------------------------*/

        final CharSequence colors[] = tipolist.toArray(new CharSequence[tipolist.size()]);


        ing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Evento.this);
                builder.setTitle("Escolha seu ingresso");
                builder.setIcon(R.drawable.cine);
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {

                        valor.setText(tipolist.get(position));
                        //seta as variaveis que vão para o carrinho
                        valoringresso = precolist.get(position);
                        idingresso = ingrelist.get(position);
                        tipoingre = tipolist2.get(position);
                        lote = lotelist.get(position);
                        auth = "1";
                    }
                });
                builder.show();
            }
        });


        //Botão promo Kplay
        tvKplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = new SweetAlertDialog(Evento.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                pDialog.setCustomImage(R.drawable.logo_kplay);
                pDialog.setTitleText("Email");
                pDialog.setContentText("contato@kplay.com.br");
                pDialog.setCancelable(true);
                pDialog.show();
                Button viewGroup = (Button) pDialog.findViewById(cn.pedant.SweetAlert.R.id.confirm_button);
                viewGroup.setVisibility(View.GONE);
            }
        });

        //Mapa
        verMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DataLongOperationAsynchTask().execute();

            }
        });


        /*---------------------------------------------
                    Add Carrinho e valida
        ----------------------------------------------*/

        //Adiciona no carrinho
        bt_addC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(auth.equals("1")){

                    new addc().execute();

                }else {

                    Toast.makeText(Evento.this, "Selecione ao menos um ingresso", Toast.LENGTH_LONG).show();

                }



            }
        });

    }

    //Ativa desincronizadamente a adição no carrinho
    private class addc extends AsyncTask<Void, Void, Void> {
        SpotsDialog dialog = new SpotsDialog(Evento.this, R.style.Custom);
        @Override
        protected void onPreExecute() {
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... v) {

            add();


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            if (Auth.equals("1")) {
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), Carrinho.class);
                startActivity(intent);

            } else if (Auth.equals("0")) {
                dialog.dismiss();
                new SweetAlertDialog(Evento.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setCustomImage(R.drawable.exclamation_icon)
                        .setTitleText("Atenção")
                        .setContentText("Deseja apagar seu carrinho, para adicionar novos itens?")
                        .setConfirmText("Sim")
                        .setCancelText("Não")

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                new removeitem().execute();
                                sweetAlertDialog.dismiss();

                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })

                        .show();

            }

            if (Auth.equals("2")) {
                dialog.dismiss();
                Toast.makeText(Evento.this, "Erro API Add Carrinho", Toast.LENGTH_LONG).show();
                onBackPressed();
            }
        }

    }


    //Metodo POST que faz o envio das infos para o carrinho
    public String add() {


        String resultado = null;

        try {
            RequestBody formBody = new FormBody.Builder()
                    .add("ingressos", idingresso)
                    .add("idevento", idevento)
                    .add("usuario", String.valueOf(session.getId()))
                    //.add("valordesconto", "0.00")
                    .add("nome", tipoingre)
                    .add("lote", lote)
                    .add("quantidade", "1")
                    .add("valor", valoringresso)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(session.getGlobalURL()+"/api/add-carrinho")
                    .post(formBody)
                    .build();

            response = client.newCall(request).execute();
            json = response.body().string();
            //Log.i("APX", json);

            try {

                // Aqui pega o retorno ({"retorno":true,"msg":[{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}]});
                JSONObject responseFull = new JSONObject(json);
                //JSONArray obj_array;

                // Aqui separo a mensagem de validação ("retorno":true);
                resultado = responseFull.getString("retorno");

                // Se for true pega dados dentro do jsonArray "EventosUsuario" - ("usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}])
                if (resultado.equals("true")) {

                    carrinhoItem();

                    Auth = "1";
                } else if (resultado.equals("false")) {

                    Auth = "0";
                }

            } catch (JSONException e) {
                Auth = "2";
                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);

            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void carrinhoItem() {


        try {

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(session.getGlobalURL()+"/api/meu-carrinho/" + session.getId() + "").build(); // ENVIANDO DADOS PARA A URL ;
            Response responses = null;
            String jsonData = null;

            try {

                responses = client.newCall(request).execute();
                jsonData = responses.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }


            try {


                // Aqui pega o retorno ({"retorno":true,"msg":[{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}]});
                JSONObject responseFull = new JSONObject(jsonData);
                //Log.i("APX", jsonData);
                JSONArray obj_array;

                // Aqui separo a mensagem de validação ("retorno":true);
                String resultado = responseFull.getString("retorno");

                // Se for true pega dados dentro do jsonArray "EventosUsuario" - ("usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}])
                if (resultado.equals("true")) {

                    obj_array = responseFull.getJSONArray("MeuCarrinho");

                    for (int i = 0; i < obj_array.length(); i++) {
                        JSONObject obj = obj_array.getJSONObject(i);

                        session.setEvetemp(obj.optString("nomeevento"));
                        session.setFotoevetemp(obj.getString("imagem"));
                        session.setVlttemp(obj.optString("vltotal"));
                        session.setFlag("0");

                    }

                } else {
                    Log.i("REX", "ERRO");
                }


            } catch (JSONException e) {
                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private class removeitem extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
        }


        @Override
        protected Void doInBackground(Void... v) {

            Removeitem();


            return null;

        }

        @Override
        protected void onPostExecute(Void result) {

            new SweetAlertDialog(Evento.this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Carrinho Apagado")
                    .setConfirmText("Ok")

                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            sweetAlertDialog.dismiss();

                        }
                    }).show();

        }
    }

    public void Removeitem() {


        try {

            OkHttpClient client = new OkHttpClient();
            //Request request = new Request.Builder().url("http://konvita.kplay.com.br/api/exclui-item-carrinho/" + carrinhoItem.getSessaocarrinho() + "").build(); // ENVIANDO DADOS PARA A URL ;
            Request request = new Request.Builder().url(session.getGlobalURL()+"/api/limpar-carrinho/" + session.getId() + "").build(); // ENVIANDO DADOS PARA A URL ;
            Response responses = null;
            String jsonData = null;

            try {

                responses = client.newCall(request).execute();
                jsonData = responses.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }


            try {


                // Aqui pega o retorno ({"retorno":true,"msg":[{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}]});
                JSONObject responseFull = new JSONObject(jsonData);
                //JSONArray obj_array ;

                // Aqui separo a mensagem de validação ("retorno":true);
                String resultado = responseFull.getString("retorno");

                // Se for true pega dados dentro do jsonArray "EventosUsuario" - ("usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}])
                if (resultado.equals("true")) {
                    session.setEvetemp("");
                    session.setFotoevetemp("");
                    session.setVlttemp("");

                } else {


                }

            } catch (JSONException e) {
                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    /*----------------------------------
                 Location
    ----------------------------------*/

    private class DataLongOperationAsynchTask extends AsyncTask<String, Void, String[]> {
        ProgressDialog dialog = new ProgressDialog(Evento.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Please wait...");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String[] doInBackground(String... params) {

            try {
                String response = getLatLongByURL("http://maps.google.com/maps/api/geocode/json?address=" + cidade + "," + estado + "&sensor=false").toLowerCase();
                Log.d("response", "" + response);
                Log.d("response", "http://maps.google.com/maps/api/geocode/json?address=" + num + "," + rua + end + "," + cidade + "," + estado + "&sensor=false");

                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");

                lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");

                Log.d("latitude", "" + lat);
                Log.d("longitude", "" + lng);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (dialog.isShowing()) {

                dialog.dismiss();

                String uri = String.format(Locale.ENGLISH, "geo:%f,%f?z=%d&q=%f,%f (%s)", lat, lng, 20, lat, lng, nome);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }

        }
    }


    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


}

