package br.com.konvita.konvita_clientes.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;


public class SplashScreen  extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
            Configurando a Splash:
                manifest/androidManifest.xml
                res/drawable/background_splash.xml
                res/values/styles.xml

        */
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        SystemClock.sleep(1000);
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
        finish();

    }
}