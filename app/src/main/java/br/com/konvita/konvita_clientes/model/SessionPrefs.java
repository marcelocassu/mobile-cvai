package br.com.konvita.konvita_clientes.model;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionPrefs {

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private final static String URL = "https://www.c-vai.com";
    private Context ctx;

    private String nome, sobrenome, cpf, rg, quantidade, mPFinal, c,
            ddd, celular, fotoperfil, email, auth, filmeid, id, flag,
            genero, nickname, dt, evetemp, fotoevetemp, vlttemp;

    public SessionPrefs(Context ctx){
        this.ctx = ctx;
        prefs = ctx.getSharedPreferences("konvita_clientes", Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public void setLoggedin(boolean logggedin){
        editor.putBoolean("loggedInmode",logggedin);
        editor.commit();
    }

    public boolean loggedin(){
        return prefs.getBoolean("loggedInmode", false);
    }

    public String getId() {
       return prefs.getString("id", id);
    }

    public void setId(String id) {
        editor.putString("id", id);
        editor.commit();
    }

    public String getQuantidade() {
        return prefs.getString("quantidade", quantidade);
    }

    public void setQuantidade(String quantidade) {
        editor.putString("quantidade", quantidade);
        editor.commit();
    }

    public String getAuth() {
        return prefs.getString("auth", auth);
    }

    public void setAuth(String auth) {
        editor.putString("auth", auth);
        editor.commit();
    }

    public String getEvetemp() {
        return prefs.getString("evetemp", evetemp);
    }

    public void setEvetemp(String evetemp) {
        editor.putString("evetemp", evetemp);
        editor.commit();
    }

    public String getFotoevetemp() {
        return prefs.getString("fotoevetemp", fotoevetemp);
    }

    public void setFotoevetemp(String fotoevetemp) {
        editor.putString("fotoevetemp", fotoevetemp);
        editor.commit();
    }
    public String getVlttemp() { return prefs.getString("vlttemp", vlttemp);
    }

    public void setVlttemp(String vlttemp) {
        editor.putString("vlttemp", vlttemp);
        editor.commit();
    }

    public String getFilmeid() {
        return prefs.getString("filmeid", filmeid);
    }

    public void setFilmeid(String filmeid) {
        editor.putString("filmeid", filmeid);
        editor.commit();
    }

    public String getNome() {
        return prefs.getString("nome", nome);
    }

    public void setNome(String nome) {
        editor.putString("nome", nome);
        editor.commit();
    }
    public String getSobrenome() {
        return prefs.getString("sobrenome", sobrenome);
    }

    public void setMPFinal(String mPFinal) {
        editor.putString("mPFinal", mPFinal);
        editor.commit();
    }
    public String getMPFinal() {
        return prefs.getString("mPFinal", mPFinal);
    }

    public void setSobrenome(String sobrenome) {
        editor.putString("sobrenome", sobrenome);
        editor.commit();
    }

    public String getCpf() {
        return prefs.getString("cpf", cpf);
    }

    public void setCpf(String cpf) {
        editor.putString("cpf", cpf);
        editor.commit();
    }

    public String getRg() {
        return prefs.getString("rg", rg);
    }

    public void setRg(String rg) {
        editor.putString("rg", rg);
        editor.commit();
    }

    public String getDdd() {
        return prefs.getString("ddd", ddd);
    }

    public void setDdd(String ddd) {
        editor.putString("ddd", ddd);
        editor.commit();
    }

    public String getCelular() {
        return prefs.getString("celular", celular);
    }

    public void setCelular(String celular) {
        editor.putString("celular", celular);
        editor.commit();
    }
    public String getFotoperfil() {
        return prefs.getString("fotoperfil", fotoperfil);
    }

    public void setFotoperfil(String fotoperfil) {
        editor.putString("fotoperfil", fotoperfil);
        editor.commit();
    }
    public String getGenero() {
        return prefs.getString("genero", genero);
    }

    public void setGenero(String genero) {
        editor.putString("genero", genero);
        editor.commit();
    }
    public String getDt() {
        return prefs.getString("dt", dt);
    }

    public void setDt(String dt) {
        editor.putString("dt", dt);
        editor.commit();
    }
    public String getEmail() {
        return prefs.getString("email", email);
    }

    public void setEmail(String email) {
        editor.putString("email", email);
        editor.commit();
    }


    public String getFlag() {
        return prefs.getString("flag", flag);
    }

    public void setFlag(String flag) {
        editor.putString("flag", flag);
        editor.commit();
    }

    public String getGlobalURL() {
        return URL;
    }

    public String getC() {
        return prefs.getString("c", c);
    }

    public void setC(String c) {
        editor.putString("c", c);
        editor.commit();
    }
}
