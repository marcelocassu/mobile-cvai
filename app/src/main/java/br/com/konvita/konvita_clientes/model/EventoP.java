package br.com.konvita.konvita_clientes.model;


import java.util.List;

public class EventoP {


    private String nome;
    private String descricao, dini, dfim, hini, hfim;
    private String image, logradouro, uf, local,
            endereco, numero, bairro, cidade, idevento, categoria;
    private String nomeEvento, nIngresso, ctgIngresso,
            tipoIngresso, numLote, qtd, preco;

    public List<Ingresso> ingressos;

    public EventoP(){

    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getDini() {
        return dini;
    }

    public void setDini(String dini) {
        this.dini = dini;
    }

    public String getDfim() {
        return dfim;
    }

    public void setDfim(String dfim) {
        this.dfim = dfim;
    }

    public String getHini() {
        return hini;
    }

    public void setHini(String hini) {
        this.hini = hini;
    }

    public String getHfim() {
        return hfim;
    }

    public void setHfim(String hfim) {
        this.hfim = hfim;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getIdevento() {
        return idevento;
    }

    public void setIdevento(String idevento) {
        this.idevento = idevento;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }





}
