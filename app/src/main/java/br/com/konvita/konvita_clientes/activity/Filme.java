package br.com.konvita.konvita_clientes.activity;


import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.adapter.sessaoFilmeAdapter;
import br.com.konvita.konvita_clientes.model.SessaoFilme;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import dmax.dialog.SpotsDialog;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Filme extends AppCompatActivity {

    TextView tvnome;
    TextView tvdescricao;
    TextView tvKplay;
    TextView tv_dhini;
    TextView tv_local;
    String nome, acessorio, url, image, idevento;
    String jsonD;
    Response response;
    sessaoFilmeAdapter adapter;
    SessionPrefs session;
    ListView Sessao;
    List<SessaoFilme> sessaoFilmeList = new ArrayList<SessaoFilme>();

    @Override
    protected void onStart() {
        super.onStart();

    new SessaoList().execute();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();


        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.activity_filme);
        } else {
            setContentView(R.layout.activity_filme);
        }

        session = new SessionPrefs(this);

        tvnome = (TextView) findViewById(R.id.nomefilme);
        tvdescricao = (TextView) findViewById(R.id.sinopse);
        ImageView banner = (ImageView) findViewById(R.id.banner);
        tvKplay = (TextView) findViewById(R.id.tvKplay);
        tv_dhini = (TextView) findViewById(R.id.diretor);
        TextView tv_dhfim = (TextView) findViewById(R.id.tela);
        TextView time = (TextView) findViewById(R.id.duracao);
        tv_local = (TextView) findViewById(R.id.classic);
        //Lista
        Sessao = (ListView) findViewById(R.id.listSessao);
        //adapter
        adapter = new sessaoFilmeAdapter(Filme.this, sessaoFilmeList);


        //Recebe os dados da Sessao
        Bundle bundle = getIntent().getExtras();
        nome = bundle.getString("nomefilme");
        String sinopse = bundle.getString("sinopse");
        image = bundle.getString("imagem");
        String classic = bundle.getString("classic");
        String diretor = bundle.getString("diretor");
        String nomeoriginal = bundle.getString("nomeoriginal");
        String tempo = bundle.getString("tempo");
        String trailer = bundle.getString("trailer");

        acessorio = session.getGlobalURL()+"/imagens/bannerfilmes/" + image;

        //URL Button
        idevento = bundle.getString("idfilm");
        url = session.getGlobalURL()+"/detalhes-eventos/" + idevento;


        //Seta as variaveis com seus devidos valores
        Picasso.with(Filme.this).load(acessorio).resize(480, 800).error(R.drawable.logo_splashscreen).into(banner);
        tvnome.setText(nome);
        tvdescricao.setText(sinopse);
        tv_dhini.setText(diretor);
        tv_dhfim.setText("3D");
        time.setText(tempo);
        if(classic.equals("0")){
            tv_local.setText("Verifique "+"\n"+" a Classificação");
        }else {
            tv_local.setText(classic + " anos");
        }


        // criando o Array de String para encher o Spinner
         /*ArrayAdapter<String> aOpcoes;
        //Cria Spinner
       Spinner spinner = (Spinner) findViewById(R.id.spinner);
        //Chamando o Banco
        db = openOrCreateDatabase("IngressoDB.db", Context.MODE_PRIVATE, null);
        //Iniciando cursor pra trazer infos
        Cursor c = db.rawQuery("SELECT * FROM Ingresso where Evento = " + idevento + "", null);
        //Verifica se está vazio
        if (c.getCount() == 0) {

            Toast.makeText(this, "Dados gravados sem sucesso", Toast.LENGTH_SHORT).show();
            c.close();

        } else {

            while (c.moveToNext()) {
                ingrelist.add(c.getString(2));
                tipolist.add(c.getString(3));
                precolist.add(c.getString(7));
                lotelist.add(c.getString(5));
            }
            //Log.i("APX", mapString.toString());
            c.close();
        }
        aOpcoes = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, tipolist);
        spinner.setAdapter(aOpcoes);

        valor.setText(precolist.get(0));



        //Recupera o clique com seleção no spinner e altera as infos
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //Valor do ingresso pro cliente
                valor.setText(precolist.get(position));
                //seta as variaveis que vão para o carrinho
                valoringresso = precolist.get(position);
                idingresso = ingrelist.get(position);
                tipoingre= tipolist.get(position);
                lote = lotelist.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        /*//Botão promo Kplay
        tvKplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = new SweetAlertDialog(Filme.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                pDialog.setCustomImage(R.drawable.logo_kplay);
                pDialog.setTitleText("Email");
                pDialog.setContentText("contato@kplay.com.br");
                pDialog.setCancelable(true);
                pDialog.show();
                Button viewGroup = (Button) pDialog.findViewById(cn.pedant.SweetAlert.R.id.confirm_button);
                viewGroup.setVisibility(View.GONE);
            }
        });*/

    }

    //Ativa desincronizadamente a adição no carrinho
    private class SessaoList extends AsyncTask<Void, Void, Void> {
        SpotsDialog dialog = new SpotsDialog(Filme.this, R.style.Custom);
        @Override
        protected void onPreExecute() {sessaoFilmeList.clear();
            dialog.setCancelable(false);
            dialog.show();}

        @Override
        protected Void doInBackground(Void... v) {
            sessaoFilme();
            return null;

        }

        @Override
        protected void onPostExecute(Void result) {

            if(dialog.isShowing()){

                Sessao.setAdapter(adapter);
                dialog.dismiss();
            }

        }

    }

    public void sessaoFilme() {



            try {

                RequestBody formBody = new FormBody.Builder()
                        .add("filme", idevento)
                        .build();

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(session.getGlobalURL()+"/api-cinema/filme/")
                        .post(formBody)
                        .build();
                try {

                response = client.newCall(request).execute();
                jsonD = response.body().string();


            } catch (IOException e) {
                e.printStackTrace();
            }

            try {

                // Aqui pega o retorno ({"retorno":true,"msg":[{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}]});
                JSONObject responseFull = new JSONObject(jsonD);
                JSONArray obj_array ;

                // Aqui separo a mensagem de validação ("retorno":true);
                String resultado = responseFull.getString("retorno");

                // Se for true pega dados dentro do jsonArray "EventosUsuario" - ("usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"},{"usuario":282,"nome":"Bruno","sobrenome":"Matheus","evento":34,"nomeEvento":"Show Oficina G3"}])
                if (resultado.equals("true")) {

                    obj_array = responseFull.getJSONArray("sessoes");

                    for (int i = 0; i < obj_array.length(); i++) {
                        JSONObject obj = obj_array.getJSONObject(i);
                        SessaoFilme sf = new SessaoFilme();
                        sf.setNomefantasia(obj.getString("nomefantasia"));
                        sf.setTela(obj.getString("tela"));
                        sf.setAudio(obj.getString("audio"));
                        sf.setDinicio(obj.getString("dinicio"));
                        sf.setHorario(obj.getString("horario"));
                        sf.setSala(obj.getString("sala"));
                        sf.setSessao(obj.getString("sessao"));
                        sessaoFilmeList.add(sf);
                    }

                } else {
                    Log.i("REX", "ERRO");
                }



            } catch (JSONException e) {
                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }



    }


}






