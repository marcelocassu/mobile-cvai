package br.com.konvita.konvita_clientes.activity;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.adapter.PolOcupadasCinemaAdapter;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import dmax.dialog.SpotsDialog;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PoltronasOcupadas extends AppCompatActivity {

    List<br.com.konvita.konvita_clientes.model.PoltronasOcupadas> poltronasOcupadasList = new ArrayList<br.com.konvita.konvita_clientes.model.PoltronasOcupadas>();
    PolOcupadasCinemaAdapter adapter;
    SessionPrefs session;
    GridView gridView;
    String sessao, json, auth, erro;
    Response response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poltronas_ocupadas);


        session = new SessionPrefs(this);
        gridView = (GridView) findViewById(R.id.lkl);
        adapter = new PolOcupadasCinemaAdapter(this, poltronasOcupadasList);
        Bundle bundle = getIntent().getExtras();
        sessao = bundle.getString("sessao");

        new LoadingBusyPol().execute();

    }

    private class LoadingBusyPol extends AsyncTask<Void, Void, Void> {
        SpotsDialog dialog = new SpotsDialog(PoltronasOcupadas.this, R.style.Custom);
        @Override
        protected void onPreExecute() {
            dialog.setCancelable(false);
            dialog.show();
        }


        @Override
        protected Void doInBackground(Void... v) {

            BusyPol();
            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            if(dialog.isShowing()){
                gridView.setAdapter(adapter);
                dialog.dismiss();
            }


        }
    }

    private String BusyPol() {

        String resultado ;

        try {
            RequestBody formBody = new FormBody.Builder()
                    .add("sessao", sessao)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(session.getGlobalURL() + "/api-cinema/assentos-ocupados")
                    .post(formBody)
                    .build();

            response = client.newCall(request).execute();
            json = response.body().string();

            try {

                JSONObject responseFull = new JSONObject(json);
                JSONArray obj_array;

                resultado = responseFull.getString("retorno");

                if (resultado.equals("true")) {


                    String resultado2 = responseFull.getString("ocupadas");
                    obj_array = new JSONArray(resultado2);
                    poltronasOcupadasList.clear();
                    for (int i = 0; i < obj_array.length(); i++) {

                        //Forma principal
                        br.com.konvita.konvita_clientes.model.PoltronasOcupadas fi = new br.com.konvita.konvita_clientes.model.PoltronasOcupadas();
                        fi.setF(obj_array.optString(i));
                        poltronasOcupadasList.add(fi);

                        //Outra Forma
                        //poltronasLivresList.add(new PoltronasLivres(obj_array.optString(i)));
                    }
                    auth = "1";
                }

            } catch (JSONException e) {
                erro = json;
                auth = "0";
                Log.e("ERRO", "Não Encontrou Nenhum JSON", e);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}


