package br.com.konvita.konvita_clientes.adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.activity.Ingressos;
import br.com.konvita.konvita_clientes.model.EventoD;
import br.com.konvita.konvita_clientes.model.Ingresso;
import br.com.konvita.konvita_clientes.model.IngressoC;
import br.com.konvita.konvita_clientes.model.SessionPrefs;

public class ingressosCAdapter extends BaseAdapter {


    private List<IngressoC> ingressos;
    private Context context;


    public ingressosCAdapter(Context c, List<IngressoC> ingressoItem) {

        this.ingressos = ingressoItem;
        this.context = c;

    }

    @Override
    public int getCount() {
        return ingressos.size(); //returns total item in the list
    }

    @Override
    public Object getItem(int position) {
        return ingressos.get(position); //returns the item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return ingressos.indexOf(getItem(position));
    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.ingressos_row, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        IngressoC currentItem = (IngressoC) getItem(position);

        if (currentItem.getIgressonome().isEmpty()){
            viewHolder.itemName.setText(currentItem.getNome());
            viewHolder.itemPol.setText("Poltrona: "+currentItem.getCadeira());
        }else{
            viewHolder.itemName.setText(currentItem.getIgressonome());
        }
        viewHolder.itemDescription.setText(currentItem.getPed_ingre());


        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Send single item click data to SingleItemView Class
                Intent intent = new Intent(context, Ingressos.class);
                //Envia as infomações
                intent.putExtra("nome", ingressos.get(position).getNome());
                intent.putExtra("valor", ingressos.get(position).getValor());
                intent.putExtra("qrcode", ingressos.get(position).getQrcode());
                intent.putExtra("logradouro", ingressos.get(position).getLogradouro());
                intent.putExtra("bairro", ingressos.get(position).getBairro());
                intent.putExtra("tipo", ingressos.get(position).getIgressonome());
                intent.putExtra("valor", ingressos.get(position).getValor());
                intent.putExtra("data", ingressos.get(position).getData());
                intent.putExtra("ass", ingressos.get(position).getCadeira());
                intent.putExtra("endereco", ingressos.get(position).getEndereco());
                intent.putExtra("num", ingressos.get(position).getNumero());
                intent.putExtra("cidade", ingressos.get(position).getCidade());
                intent.putExtra("uf", ingressos.get(position).getUf());
                context.startActivity(intent);

            }
        });

        return convertView;
    }



    //ViewHolder inner class
    private class ViewHolder {
        TextView itemName;
        TextView itemDescription;
        TextView itemPol;


        public ViewHolder(View view) {
            itemName = (TextView) view.findViewById(R.id.tv_nome);
            itemDescription = (TextView) view.findViewById(R.id.tv_descricao);
            itemPol = (TextView) view.findViewById(R.id.tv_pol);

        }
    }
}




