package br.com.konvita.konvita_clientes.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.model.PoltronasLivres;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static br.com.konvita.konvita_clientes.R.layout.fileira_cine_row;


public class fileiraCinemaAdapter extends BaseAdapter implements Filterable {

    private CustomFilter filter;
    private List<PoltronasLivres> filterList;
    private SweetAlertDialog SAD;
    private Response response;
    private String json, erro, auth, addorRemove;
    private List<PoltronasLivres> poltronasLivresList;
    private Context context;


    public fileiraCinemaAdapter(Context c, List<PoltronasLivres> poltronasLivresList) {
        this.poltronasLivresList = poltronasLivresList;
        this.filterList = poltronasLivresList;
        this.context = c;

    }

    @Override
    public int getCount() {
        return poltronasLivresList.size();
    }

    @Override
    public Object getItem(int position) {
        return poltronasLivresList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return poltronasLivresList.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(fileira_cine_row, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        final PoltronasLivres currentItem = (PoltronasLivres) getItem(position);
        final SessionPrefs session = new SessionPrefs(context);
        final ViewHolder CheckViewHolder = viewHolder;
        CheckViewHolder.l.setBackgroundColor(Color.parseColor("#00bb2d"));

        viewHolder.itemFileira.setText(currentItem.getF());


        class addoremovePol extends AsyncTask<Void, Void, Void> {
        SpotsDialog dialog = new SpotsDialog(context, R.style.Custom);

            @Override
            protected void onPreExecute() {
                dialog.setCancelable(false);
                dialog.show();
            }

            @Override
            protected Void doInBackground(Void... v) {

                //Adiciona
                if (addorRemove.equals("1")) {


                    String resultado = null;

                    try {
                        RequestBody formBody = new FormBody.Builder()
                                .add("sessao", session.getC())
                                .add("usuario", session.getId())
                                .add("cadeira", currentItem.getF())
                                .build();


                        OkHttpClient client = new OkHttpClient();
                        Request request = new Request.Builder()
                                .url(session.getGlobalURL() + "/api-cinema/adiciona-assento")
                                .post(formBody)
                                .build();

                        response = client.newCall(request).execute();
                        json = response.body().string();

                     try {

                            JSONObject responseFull = new JSONObject(json);

                            resultado = responseFull.getString("retorno");

                            if (resultado.equals("true")) {

                                auth = "1";
                            }else if(resultado.equals("false")){

                             auth = "0";
                         }
                        } catch (JSONException e) {
                            erro = json;
                            auth = "2";
                            Log.e("ERRO", "Não Encontrou Nenhum JSON", e);

                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //Remove
                 }else if(addorRemove.equals("2")){
                    String resultado = null;

                    try {
                        RequestBody formBody = new FormBody.Builder()
                                .add("sessao", session.getC())
                                .add("cadeira", currentItem.getF())
                                .build();


                        OkHttpClient client = new OkHttpClient();
                        Request request = new Request.Builder()
                                .url(session.getGlobalURL() + "/api-cinema/remover-poltronas")
                                .post(formBody)
                                .build();

                        response = client.newCall(request).execute();
                        json = response.body().string();

                        try {

                            JSONObject responseFull = new JSONObject(json);

                            resultado = responseFull.getString("retorno");

                            if (resultado.equals("true")) {

                                auth = "1";


                            } else if(resultado.equals("false")){

                                auth = "0";
                            }
                        }catch (JSONException e) {
                            erro = json;
                            auth = "2";
                            Log.e("ERRO", "Não Encontrou Nenhum JSON", e);

                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

               } return null;

            }

            @Override
            protected void onPostExecute(Void result) {
                if(dialog.isShowing()){
                    dialog.dismiss();
                }
                if(auth.equals("0")){
                    SAD = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                    SAD
                            .setCustomImage(R.drawable.exclamation_icon)
                            .setTitleText("Atenção")
                            .setContentText("Essa poltrona está indisponível")
                            .setConfirmText("Entendi")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    CheckViewHolder.l.setBackgroundColor(Color.parseColor("#00bb2d"));
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                    SAD.setCancelable(false);

                }else if(auth.equals("2")){
                    CheckViewHolder.l.setBackgroundColor(Color.parseColor("#00bb2d"));
                    Toast.makeText(context, "Sistema em Manutenção", Toast.LENGTH_SHORT).show();
                }
            }
        }


        viewHolder.l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int color = Color.TRANSPARENT;
                Drawable background = view.getBackground();
                if (background instanceof ColorDrawable)
                    color = ((ColorDrawable) background).getColor();

                //Log.i("APX", String.valueOf(color));

                if (color == 0 || color == -16729299) {
                    CheckViewHolder.l.setBackgroundColor(Color.parseColor("#0e4bef"));
                    //Toast.makeText(context, "Poltrona " + currentItem.getF() + " Selecionado", Toast.LENGTH_SHORT).show();
                    new addoremovePol().execute();
                    addorRemove = "1";

                } else if (color == -15840273) {
                    CheckViewHolder.l.setBackgroundColor(Color.parseColor("#00bb2d"));
                    //Toast.makeText(context, "Poltrona " + currentItem.getF() + " Deselecionada", Toast.LENGTH_SHORT).show();
                    new addoremovePol().execute();
                    addorRemove = "2";
                }

            }
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {

        if (filter == null) {
            filter = new CustomFilter();
        }
        return filter;
    }

    //Custom Filter Inner Class
    private class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                //CONSTARINT TO UPPER
                constraint = constraint.toString().toUpperCase();
                final ArrayList<PoltronasLivres> filters = new ArrayList<>();


                filters.clear();
                //get specific items
                for (int i = 0; i < filterList.size(); i++) {
                    if (filterList.get(i).getF().toUpperCase().contains(constraint)) {
                        filters.add(filterList.get(i));
                    }
                }
                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = filterList.size();
                results.values = filterList;

            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            poltronasLivresList = (ArrayList<PoltronasLivres>) results.values;

            notifyDataSetChanged();
        }
    }

    //ViewHolder inner class
    private class ViewHolder {
        TextView itemFileira;
        LinearLayout l;

        private ViewHolder(View view) {

            itemFileira = (TextView) view.findViewById(R.id.itemFileira);
            l = (LinearLayout) view.findViewById(R.id.l);


        }
    }

}

