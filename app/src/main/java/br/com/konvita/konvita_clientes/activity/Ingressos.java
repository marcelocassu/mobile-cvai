package br.com.konvita.konvita_clientes.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.model.SessionPrefs;

public class Ingressos extends AppCompatActivity {

    TextView tipo1, nomeevento, data1,
            valor1, assento1,local;
    ImageView banner, Qr_img;
    SessionPrefs sessionPrefs;
    RelativeLayout container;
    String nome, cidade, estado;
    double lat, lng;
    Button qr1;
    boolean show;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Sessão que recupera infos do usuario
        sessionPrefs = new SessionPrefs(this);

        //Bundle de infos do evento
        Bundle bundle = getIntent().getExtras();
        nome = bundle.getString("nome");
        String qrcode = bundle.getString("qrcode");
        String tipo = bundle.getString("tipo");
        String valor = bundle.getString("valor");
        String assento = bundle.getString("ass");
        String data = bundle.getString("data");
        String logradouro = bundle.getString("logradouro");
        String endereco = bundle.getString("endereco");
        String numero = bundle.getString("num");
        String bairro = bundle.getString("bairro");
        cidade = bundle.getString("cidade");
        estado = bundle.getString("uf");


        //Infos evento
        if(!nome.isEmpty()){
            setContentView(R.layout.activity_ingressos_c);
            //Qr
            container = (RelativeLayout) findViewById(R.id.vv);
            container.setVisibility(View.INVISIBLE);
            banner = (ImageView) findViewById(R.id.banner);
            Qr_img = (ImageView) findViewById(R.id.qr_img);
            qr1 = (Button) findViewById(R.id.showqr);


            //info do evento
            nomeevento = (TextView) findViewById(R.id.tv_nomeevento);
            tipo1 = (TextView) findViewById(R.id.tipo);
            valor1 = (TextView) findViewById(R.id.valor);
            local = (TextView) findViewById(R.id.local);
            data1 = (TextView) findViewById(R.id.data);
            assento1 = (TextView) findViewById(R.id.assento);


            nomeevento.setText(nome);
            valor1.setText("R$ "+valor);
            assento1.setText(assento);
            data1.setText(data);
            String endcompleto = logradouro + " " + endereco + "\n" + numero + ", " + bairro + "\n" + cidade + "-" + estado;
            local.setText(endcompleto);


            //Seta a imagem
            Picasso.with(Ingressos.this).load(qrcode).resize(200, 200).error(R.drawable.ic_launcher).into(Qr_img);



            //Esconde aparece o QRCODE
            qr1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // container.setVisibility(View.VISIBLE);
                    if (show) {
                        container.setVisibility(View.INVISIBLE);
                        show = false;
                    } else {
                        container.setVisibility(View.VISIBLE);
                        show = true;
                    }
                }
            });


        }else {
            setContentView(R.layout.activity_ingressos);
            //Qr
            container = (RelativeLayout) findViewById(R.id.vv);
            container.setVisibility(View.INVISIBLE);
            banner = (ImageView) findViewById(R.id.banner);
            Qr_img = (ImageView) findViewById(R.id.qr_img);
            qr1 = (Button) findViewById(R.id.showqr);


            //info do evento
            tipo1 = (TextView) findViewById(R.id.tipo);
            valor1 = (TextView) findViewById(R.id.valor);
            local = (TextView) findViewById(R.id.local);
            data1 = (TextView) findViewById(R.id.data);


            valor1.setText("R$ "+valor);
            tipo1.setText(tipo);
            data1.setText(data);
            String endcompleto = logradouro + " " + endereco + "\n" + numero + ", " + bairro + "\n" + cidade + " - " + estado;
            local.setText(endcompleto);


            //Seta a imagem
            Picasso.with(Ingressos.this).load(qrcode).resize(200, 200).error(R.drawable.ic_launcher).into(Qr_img);



            //Esconde aparece o QRCODE
            qr1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // container.setVisibility(View.VISIBLE);
                    if (show) {
                        container.setVisibility(View.INVISIBLE);
                        show = false;
                    } else {
                        container.setVisibility(View.VISIBLE);
                        show = true;
                    }
                }
            });

        }


    }


    private class DataLongOperationAsynchTask extends AsyncTask<String, Void, String[]> {
        ProgressDialog dialog = new ProgressDialog(Ingressos.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Please wait...");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String[] doInBackground(String... params) {

            try {
                String response = getLatLongByURL("http://maps.google.com/maps/api/geocode/json?address=" + cidade + "," + estado + "&sensor=false").toLowerCase();
                // Log.d("response", "" + response);
                // Log.d("response", "http://maps.google.com/maps/api/geocode/json?address=" + num + "," + rua + end + "," + cidade + "," + estado + "&sensor=false");

                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");

                lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");

                Log.d("latitude", "" + lat);
                Log.d("longitude", "" + lng);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (dialog.isShowing()) {

                dialog.dismiss();

                String uri = String.format(Locale.ENGLISH, "geo:%f,%f?z=%d&q=%f,%f (%s)", lat, lng, 30, lat, lng, nome);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }

        }
    }


    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
