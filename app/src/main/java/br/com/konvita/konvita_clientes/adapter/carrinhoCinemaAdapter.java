package br.com.konvita.konvita_clientes.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.model.CarrinhoItem;

import static br.com.konvita.konvita_clientes.R.layout.carrinho_cinema_row;


public class carrinhoCinemaAdapter extends BaseAdapter {


    private List<CarrinhoItem> carrinhoList;
    private Context context;


    public carrinhoCinemaAdapter(Context c, List<CarrinhoItem> carrinhoItem) {
        this.carrinhoList = carrinhoItem;
        this.context = c;

    }

    @Override
    public int getCount() {
        return carrinhoList.size();
    }

    @Override
    public Object getItem(int position) {
        return carrinhoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return carrinhoList.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(carrinho_cinema_row, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        CarrinhoItem currentItem = (CarrinhoItem) getItem(position);

        viewHolder.itemCadeira.setText(currentItem.getCadeira());


        return convertView;
    }


    //ViewHolder inner class
    private class ViewHolder {
        TextView itemCadeira;

        private ViewHolder(View view) {

            itemCadeira = (TextView) view.findViewById(R.id.cadeira);



        }
    }
}

