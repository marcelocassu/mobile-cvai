package br.com.konvita.konvita_clientes.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.activity.PoltronasOcupadas;
import br.com.konvita.konvita_clientes.model.PoltronasLivres;
import br.com.konvita.konvita_clientes.model.SessionPrefs;
import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static br.com.konvita.konvita_clientes.R.layout.fileira_cine_row;


public class PolOcupadasCinemaAdapter extends BaseAdapter {



    private List<br.com.konvita.konvita_clientes.model.PoltronasOcupadas> poltronasOcupadasList;
    private Context context;


    public PolOcupadasCinemaAdapter(Context c, List<br.com.konvita.konvita_clientes.model.PoltronasOcupadas> poltronasOcupadasList) {
        this.poltronasOcupadasList = poltronasOcupadasList;
        this.context = c;

    }

    @Override
    public int getCount() {
        return poltronasOcupadasList.size();
    }

    @Override
    public Object getItem(int position) {
        return poltronasOcupadasList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return poltronasOcupadasList.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(fileira_cine_row, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        br.com.konvita.konvita_clientes.model.PoltronasOcupadas currentItem = (br.com.konvita.konvita_clientes.model.PoltronasOcupadas) getItem(position);


        viewHolder.l.setBackgroundColor(Color.RED);

        viewHolder.itemFileira.setText(currentItem.getF());

        return convertView;
    }



    //ViewHolder inner class
    private class ViewHolder {
        TextView itemFileira;
        LinearLayout l;

        private ViewHolder(View view) {

            itemFileira = (TextView) view.findViewById(R.id.itemFileira);
            l = (LinearLayout) view.findViewById(R.id.l);


        }
    }

}

