package br.com.konvita.konvita_clientes.activity;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.model.SessionPrefs;

public class FinalPagamentoMP extends AppCompatActivity {

    TextView done, tv_evento, tv_valortt, tv_parcelas, tv_cartao, tv_email, tv_numcop, tv_status;
    LinearLayout ll;
    String evento, valortt, parcelas, cartao, numcop, status;
    SessionPrefs session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new SessionPrefs(this);
        Bundle bundle = getIntent().getExtras();
        evento = bundle.getString("evento");
        valortt = bundle.getString("valortotal");
        parcelas = bundle.getString("parcelas");
        cartao = bundle.getString("cartao");
        numcop = bundle.getString("nc");
        status = bundle.getString("status");


        switch (status) {

            case "approved":
                setContentView(R.layout.activity_congrats);
                tv_evento = (TextView) findViewById(R.id.mpsdkPaymentIdDescription);
                tv_valortt = (TextView) findViewById(R.id.mpsdkInstallmentsTotalAmountDescription);
                tv_parcelas = (TextView) findViewById(R.id.mpsdkInstallmentsDescription);
                tv_cartao = (TextView) findViewById(R.id.mpsdkStateDescription);
                tv_email = (TextView) findViewById(R.id.mpsdkPayerEmail);
                //tv_status = (TextView) findViewById(R.id.mpsdkCongratulationsSubtitle);
                tv_numcop = (TextView) findViewById(R.id.mpsdkPaymentIdDescriptionNumber);
                done = (TextView) findViewById(R.id.done);
                ll = (LinearLayout) findViewById(R.id.mpsdkTitleBackground);


                tv_evento.setText(evento);
                if (cartao.equals("visa")) {
                    ImageView card = (ImageView) findViewById(R.id.mpsdkPaymentMethodImage);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        card.setBackground(getDrawable(R.drawable.mpsdk_visa));
                    }
                }
                //tv_cartao.setText(cartao);
                tv_parcelas.setText(parcelas + " X " + valortt);
                tv_valortt.setText(valortt);
                tv_email.setText("Eviaremos os dados para \n" + session.getEmail());
                //tv_status.setText(status);
                tv_numcop.setText("Comprovante N° " + numcop);

                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onBackPressed();
                        session.setMPFinal("0");
                        finish();
                    }
                });

                break;

            case "rejected":

                setContentView(R.layout.activity_error);
                tv_evento = (TextView) findViewById(R.id.mpsdkPaymentIdDescription);
                tv_valortt = (TextView) findViewById(R.id.mpsdkInstallmentsTotalAmountDescription);
                tv_parcelas = (TextView) findViewById(R.id.mpsdkInstallmentsDescription);
                tv_cartao = (TextView) findViewById(R.id.mpsdkStateDescription);
                //tv_email = (TextView) findViewById(R.id.mpsdkPayerEmail);
                //tv_status = (TextView) findViewById(R.id.mpsdkCongratulationsSubtitle);
                tv_numcop = (TextView) findViewById(R.id.mpsdkPaymentIdDescriptionNumber);
                done = (TextView) findViewById(R.id.done);
                ll = (LinearLayout) findViewById(R.id.mpsdkTitleBackground);
                tv_evento.setText(evento);

                if (cartao.equals("visa")) {
                    ImageView card = (ImageView) findViewById(R.id.mpsdkPaymentMethodImage);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        card.setBackground(getDrawable(R.drawable.mpsdk_visa));
                    }
                }

                //tv_cartao.setText(cartao);
                tv_parcelas.setText(parcelas + " X " + valortt);
                tv_valortt.setText(valortt);
                tv_email.setText("Eviaremos os dados para \n" + session.getEmail());
                //tv_status.setText(status);
                tv_numcop.setText("Comprovante N° " + numcop);

                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onBackPressed();
                        session.setMPFinal("0");
                        finish();
                    }
                });
                break;

            case "in_process":

                setContentView(R.layout.activity_in_process);
                tv_evento = (TextView) findViewById(R.id.mpsdkPaymentIdDescription);
                tv_valortt = (TextView) findViewById(R.id.mpsdkInstallmentsTotalAmountDescription);
                tv_parcelas = (TextView) findViewById(R.id.mpsdkInstallmentsDescription);
                tv_cartao = (TextView) findViewById(R.id.mpsdkStateDescription);
                tv_email = (TextView) findViewById(R.id.mpsdkPayerEmail);
                //tv_status = (TextView) findViewById(R.id.mpsdkCongratulationsSubtitle);
                tv_numcop = (TextView) findViewById(R.id.mpsdkPaymentIdDescriptionNumber);
                done = (TextView) findViewById(R.id.done);
                ll = (LinearLayout) findViewById(R.id.mpsdkTitleBackground);


                tv_evento.setText(evento);
                if (cartao.equals("visa")) {
                    ImageView card = (ImageView) findViewById(R.id.mpsdkPaymentMethodImage);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        card.setBackground(getDrawable(R.drawable.mpsdk_visa));
                    }
                }
                //tv_cartao.setText(cartao);
                tv_parcelas.setText(parcelas + " X " + valortt);
                tv_valortt.setText(valortt);
                tv_email.setText("Eviaremos os dados para \n" + session.getEmail());
                //tv_status.setText(status);
                tv_numcop.setText("Comprovante N° " + numcop);

                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onBackPressed();
                        session.setMPFinal("0");
                        finish();
                    }
                });
                break;
        }



    }

}
