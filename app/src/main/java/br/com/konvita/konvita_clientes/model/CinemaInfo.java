package br.com.konvita.konvita_clientes.model;


public class CinemaInfo {

    private String filmeid, tempo;
    private String sinopse, tela, trailer, nome;
    private String foto, diretor, classific, nomeoriginal;
            //endereco, numero, bairro, cidade, idevento;

    public CinemaInfo(){

    }

    public String getFilmeid() {
        return filmeid;
    }

    public void setFilmeid(String filmeid) {
        this.filmeid = filmeid;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getTela() {
        return tela;
    }

    public void setTela(String tela) {
        this.tela = tela;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer (String trailer) {
        this.trailer = trailer;
    }

    public String getDiretor() {
        return diretor;
    }

    public void setDiretor(String diretor) {
        this.diretor = diretor;
    }

    public String getClassific() {
        return classific;
    }

    public void setClassific(String classific) {
        this.classific = classific;
    }

    public String getNomeoriginal() {
        return nomeoriginal;
    }

    public void setNomeoriginal(String nomeoriginal) {
        this.nomeoriginal = nomeoriginal;
    }

    public String getTempo() {
        return tempo;
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
    }

}
