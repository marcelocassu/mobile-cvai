package br.com.konvita.konvita_clientes.adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


import br.com.konvita.konvita_clientes.R;
import br.com.konvita.konvita_clientes.activity.EventosCIngressos;
import br.com.konvita.konvita_clientes.model.EventoC;
import br.com.konvita.konvita_clientes.model.SessionPrefs;

public class eventosAdapterComprados extends BaseAdapter implements Filterable {


    private CustomFilter filter;
    private List<EventoC> filterList;
    private List<EventoC> eventos;
    private Context context;


    public eventosAdapterComprados(Context c, List<EventoC> eventosItem) {
        this.filterList = eventosItem;
        this.eventos = eventosItem;
        this.context = c;

    }

    @Override
    public int getCount() {
        return eventos.size(); //returns total item in the list
    }

    @Override
    public Object getItem(int position) {
        return eventos.get(position); //returns the item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return eventos.indexOf(getItem(position));
    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

       ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.eventos_row, parent, false);
            viewHolder = new eventosAdapterComprados.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (eventosAdapterComprados.ViewHolder) convertView.getTag();
        }

        final EventoC currentItem = (EventoC) getItem(position);
        final SessionPrefs session = new SessionPrefs(context);
        viewHolder.itemName.setText(currentItem.getNome());
        viewHolder.itemDescription.setText(currentItem.getCategoria());

        if (!currentItem.getEvento().isEmpty()) {
            Picasso.with(context).load(session.getGlobalURL() + "/imagens/bannereventos/" + currentItem.getImage())
                    .error(R.drawable.error)
                    .into(viewHolder.avatar);
                    session.setFlag("0");

        }else{
            Picasso.with(context).load(session.getGlobalURL() + "/imagens/bannerfilmes/" + currentItem.getImage())
                    .error(R.drawable.error)
                    .into(viewHolder.avatar);
                    session.setFlag("1");
        }

        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Send single item click data to SingleItemView Class
                Intent intent = new Intent(context, EventosCIngressos.class);
                //Envia as infomações
                if (eventos.get(position).getSessao().isEmpty()){
                    intent.putExtra("nomeevento",  eventos.get(position).getNome());
                    String image = session.getGlobalURL()+"/imagens/bannereventos/"+eventos.get(position).getImage();
                    intent.putExtra("image", image);
                    session.setFlag("1");
                    intent.putExtra("es", eventos.get(position).getEvento());
                    intent.putExtra("logradouro", eventos.get(position).getLogradouro());
                    intent.putExtra("endereco", eventos.get(position).getEndereco());
                    intent.putExtra("numero", eventos.get(position).getNumero());
                    intent.putExtra("bairro", eventos.get(position).getBairro());
                    intent.putExtra("cidade", eventos.get(position).getCidade());
                    intent.putExtra("uf", eventos.get(position).getUf());

                }else {
                    intent.putExtra("nomeevento",  eventos.get(position).getNome());
                    String image = session.getGlobalURL()+"/imagens/bannerfilmes/"+eventos.get(position).getImage();
                    intent.putExtra("image", image);
                    session.setFlag("0");
                    intent.putExtra("es", eventos.get(position).getSessao());
                    intent.putExtra("logradouro", eventos.get(position).getLogradouro());
                    intent.putExtra("endereco", eventos.get(position).getEndereco());
                    intent.putExtra("numero", eventos.get(position).getNumero());
                    intent.putExtra("bairro", eventos.get(position).getBairro());
                    intent.putExtra("cidade", eventos.get(position).getCidade());
                    intent.putExtra("uf", eventos.get(position).getUf());
                }

                context.startActivity(intent);

            }
        });


        return convertView;
    }

    @Override
    public Filter getFilter() {

        if (filter == null) {
            filter = new eventosAdapterComprados.CustomFilter();
        }
        return filter;
    }

    //Custom Filter Inner Class
    private class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                //CONSTARINT TO UPPER
                constraint = constraint.toString().toUpperCase();
                final ArrayList<EventoC> filters = new ArrayList<>();


                filters.clear();
                //get specific items
                for (int i = 0; i < filterList.size(); i++) {
                    if (filterList.get(i).getNome().toUpperCase().contains(constraint)) {
                        filters.add(filterList.get(i));
                    }
                }
                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = filterList.size();
                results.values = filterList;

            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            eventos = (ArrayList<EventoC>) results.values;

            notifyDataSetChanged();
        }
    }


    //ViewHolder inner class
    private class ViewHolder {
        TextView itemName;
        TextView itemDescription;
        ImageView avatar;

        public ViewHolder(View view) {
            itemName = (TextView) view.findViewById(R.id.tv_nome);
            itemDescription = (TextView) view.findViewById(R.id.tv_descricao);
            avatar = (ImageView) view.findViewById(R.id.post_avatar);
        }
    }
}

